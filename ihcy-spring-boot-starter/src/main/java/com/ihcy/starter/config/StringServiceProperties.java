package com.ihcy.starter.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("string.service")
public class StringServiceProperties {

    /**
     * @ConfigurationProperties("string.service") 执行配置属性的前缀
     * string.service.prefix
     * string.service.suffix
     */

    /**
     *  string.service.prefix
     */
    private String prefix;

    /**
     *  string.service.suffix
     */
    private String suffix;

    /**
     * TODO 如何设置默认提示值
     */
    private String enable;
}
