package com.ihcy.starter.config;


import com.ihcy.starter.service.StringService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnClass(StringService.class)
@EnableConfigurationProperties(StringServiceProperties.class)
public class StringServiceAutoConfiguration {
    /**
     * @ConditionalOnClass(StringService.class)
     * StringService 存在启动这个自动加载类
     *
     * @EnableConfigurationProperties(StringServiceProperties.class)
     * 读取StringServiceProperties 属性
     */

    @Autowired
    private StringServiceProperties properties;

    /**
     * some.service.enable 没有赋值或者为true ，调用 getDefaultStringService
     * 不然调用 getStringService
     */

    @Bean
    @ConditionalOnProperty(name = "string.service.enable",
    havingValue = "true",
    matchIfMissing = true)
    public StringService getDefaultStringService(){
        return new StringService(properties.getPrefix(),properties.getSuffix());
    }
    @Bean
    @ConditionalOnMissingBean
    public StringService getStringService(){
        return new StringService("","");
    }
}
