package com.ihcy.starter.service;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class StringService {

    private String prefix;

    private String suffix;

    /**
     * 给String添加前后缀
     */
    public String spliceString(String word) {
        return prefix + word + suffix; 
    }
}
