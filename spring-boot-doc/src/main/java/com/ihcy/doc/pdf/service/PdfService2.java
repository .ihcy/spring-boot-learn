package com.ihcy.doc.pdf.service;

import com.ihcy.doc.pdf.builder.CellBuilder;
import com.ihcy.doc.pdf.builder.FontBuilder;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;


@Service
@Slf4j
public class PdfService2 {


    public void createPdf() {


        /**
         * 指定页面大小为A4，且自定义页边距(marginLeft、marginRight、marginTop、marginBottom)
         * Document document =new Document(PageSize.A4,50,50,30,20);
         */
        // 横版A4
        Document document = new Document(new RectangleReadOnly(842, 595, 90));
        PdfWriter writer = null;
        try {
            // 创建PdfWriter对象
            writer = PdfWriter.getInstance(document, new FileOutputStream("test2.PDF"));
            document.addAuthor("杨波");
            document.addCreationDate();
            document.addKeywords("测试");
            document.addProducer();
            document.addCreator("www.ydc51.com");
            document.addTitle("测试标题");
            document.addSubject("测试PDF创建的主题");
            document.open();
            //添加一个内容段落 ,包含字体
            Paragraph titleParagraph =  new Paragraph("付款申请单", FontBuilder.fontBuild(20, Font.BOLD));
            // 居中对齐
            titleParagraph.setAlignment(Element.ALIGN_CENTER);
            titleParagraph.setSpacingAfter(12);
            document.add(titleParagraph);
            document.add(getPdfPTable());
            document.add( new Paragraph("付款申请单", FontBuilder.fontBuild(20, Font.BOLD)));

        } catch (Exception e) {
            log.error("", e);
        } finally {
            // 关闭文档
            document.close();
            assert writer != null;
            writer.close();
        }

    }

    private PdfPTable getPdfPTable() throws DocumentException, IOException {
        // 7列的表.
        PdfPTable table = new PdfPTable(7);
        // 宽度100%填充
        table.setWidthPercentage(100);
        // 前间距
        table.setSpacingBefore(10f);
        // 后间距
        table.setSpacingAfter(10f);
        List<PdfPRow> listRow = table.getRows();
        //设置列宽
        float[] columnWidths = {1f, 1f, 1f,1f,1f,1f,1f};
        table.setWidths(columnWidths);

        //行1
        PdfPCell cells1[] = new PdfPCell[7];
        PdfPRow row1 = new PdfPRow(cells1);
        CellBuilder cellBuilder = new CellBuilder();
        //单元格
        cells1[0] = cellBuilder.buildCell("111",FontBuilder.fontBuild(11));
        cells1[1] = cellBuilder.buildCell("222",FontBuilder.fontBuild(11));
        cells1[2] = cellBuilder.buildCell("3333",FontBuilder.fontBuild(11));
        cells1[3] = cellBuilder.buildCell("444",FontBuilder.fontBuild(11));
        cells1[4] = cellBuilder.buildCell("555",FontBuilder.fontBuild(11));
        cells1[5] = cellBuilder.buildCell("6666",FontBuilder.fontBuild(11));
        cells1[6] = cellBuilder.buildCell("7777",FontBuilder.fontBuild(11));


        //行2
        PdfPCell cells2[] = new PdfPCell[7];
        PdfPRow row2 = new PdfPRow(cells2);
        cells2[0] = cellBuilder.buildCell("qweqeqeeqe",FontBuilder.fontBuild(11));
        cells2[1] = cellBuilder.buildCell("qweqeqeeqe",FontBuilder.fontBuild(11));
        cells2[2] = cellBuilder.buildCell("qweqeqeeqe",FontBuilder.fontBuild(11));
        cells2[3] = cellBuilder.buildCell("qweqeqeeqe",FontBuilder.fontBuild(11));
        cells2[4] = cellBuilder.buildCell("qweqeqeeqe",FontBuilder.fontBuild(11));
        cells2[5] = cellBuilder.buildCell("qweqeqeeqe",FontBuilder.fontBuild(11));
        cells2[6] = cellBuilder.buildCell("qweqeqeeqe",FontBuilder.fontBuild(11));
        //把第一行添加到集合
        listRow.add(row1);
        listRow.add(row2);
        return table;
    }



}

