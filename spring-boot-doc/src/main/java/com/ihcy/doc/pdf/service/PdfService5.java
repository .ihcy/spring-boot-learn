package com.ihcy.doc.pdf.service;

import com.ihcy.doc.pdf.builder.CellBuilder;
import com.ihcy.doc.pdf.builder.FontBuilder;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;


@Service
@Slf4j
public class PdfService5 {


    public void createPdf() {


        /**
         * 指定页面大小为A4，且自定义页边距(marginLeft、marginRight、marginTop、marginBottom)
         * Document document =new Document(PageSize.A4,50,50,30,20);
         */
        // 横版A4
        Document document = new Document (new RectangleReadOnly (842, 595, 90));
        PdfWriter writer = null;
        try {
            // 创建PdfWriter对象
            writer = PdfWriter.getInstance (document, new FileOutputStream ("test5.PDF"));
            document.addAuthor ("杨波");
            document.addCreationDate ();
            document.addKeywords ("测试");
            document.addProducer ();
            document.addCreator ("www.ydc51.com");
            document.addTitle ("测试标题");
            document.addSubject ("测试PDF创建的主题");
            document.open ();
            //添加一个内容段落 ,包含字体
            Paragraph titleParagraph = new Paragraph ("付款申请单", FontBuilder.fontBuild (20, Font.BOLD));
            // 居中对齐
            titleParagraph.setAlignment (Element.ALIGN_CENTER);
            titleParagraph.setSpacingAfter (12);

            // 获取图片
            ByteArrayOutputStream baos1 = new ByteArrayOutputStream ();
            InputStream inputStream1 = new ClassPathResource ("template/1.png").getInputStream ();
            BufferedImage bufferedImage1 = ImageIO.read (inputStream1);
            ImageIO.write (bufferedImage1, "png", baos1);
            Image image1 = Image.getInstance (baos1.toByteArray ());
            // （以左下角为原点）设置图片的坐标
            image1.setAbsolutePosition (10, 20);
            document.add (image1);


            // 获取图片
            ByteArrayOutputStream baos2 = new ByteArrayOutputStream ();
            InputStream inputStream2 = new ClassPathResource ("template/2.png").getInputStream ();
            BufferedImage bufferedImage2 = ImageIO.read (inputStream2);
            ImageIO.write (bufferedImage2, "png", baos2);
            Image image2 = Image.getInstance (baos2.toByteArray ());
            // （以左下角为原点）设置图片的坐标
            image2.setAbsolutePosition (40, 60);
            document.add (image2);
            document.add (titleParagraph);
            document.add (getPdfPTable ());
            document.add (new Paragraph ("付款申请单", FontBuilder.fontBuild (20, Font.BOLD)));

        } catch (Exception e) {
            log.error ("", e);
        } finally {
            // 关闭文档
            document.close ();
            assert writer != null;
            writer.close ();
        }

    }

    private PdfPTable getPdfPTable() throws DocumentException, IOException {
        // 7列的表.
        PdfPTable table = new PdfPTable(7);
        // 宽度100%填充
        table.setWidthPercentage(100);
        // 前间距
        table.setSpacingBefore(10f);
        // 后间距
        table.setSpacingAfter(5f);

        //设置列宽
        float[] columnWidths = {1f, 1f, 1f, 1f, 1f, 1f, 1f};
        table.setWidths(columnWidths);
        Font BOLDFont = FontBuilder.fontBuild(10, Font.BOLD);
        Font NormalFont = FontBuilder.fontBuild(10);
        CellBuilder cellBuilder_28 = new CellBuilder(21);

        table.addCell(cellBuilder_28.buildCell("申请主体", BOLDFont, Boolean.TRUE));

        table.addCell(cellBuilder_28.buildCell("上海慎则化工科技有限公司",
                FontBuilder.fontBuild(11, Font.BOLD | Font.UNDERLINE),2,Boolean.TRUE));

        table.addCell(cellBuilder_28.buildCell("",
                FontBuilder.fontBuild(11 ),2,Boolean.TRUE));

        table.addCell(cellBuilder_28.buildCell("付款单号", BOLDFont,Boolean.TRUE));

        table.addCell(cellBuilder_28.buildCell("1234567888",
                FontBuilder.fontBuild(11),Boolean.TRUE));

        table.addCell(cellBuilder_28.buildCell("收款单位", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("测试单位", NormalFont,2));
        table.addCell(cellBuilder_28.buildCell("申请日期", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("2020-01-14", NormalFont));
        table.addCell(cellBuilder_28.buildCell("最后付款日", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("2020-02-20", NormalFont));

        table.addCell(cellBuilder_28.buildCell("收款银行", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("测试银行", NormalFont,2));
        table.addCell(cellBuilder_28.buildCell("成本类型", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("正常成本", NormalFont));
        table.addCell(cellBuilder_28.buildCell("币   种", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("人民币", NormalFont));

        table.addCell(cellBuilder_28.buildCell("收款帐号", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("test111111111", NormalFont,2));
        table.addCell(cellBuilder_28.buildCell("原币总额", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("100,000", NormalFont));
        table.addCell(cellBuilder_28.buildCell("本币总额", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("80,000", NormalFont));

        // 获取图片
        ByteArrayOutputStream baos1 = new ByteArrayOutputStream ();
        InputStream inputStream1 = new ClassPathResource ("template/oseal.jpg").getInputStream ();
        BufferedImage bufferedImage1 = ImageIO.read (inputStream1);
        ImageIO.write (bufferedImage1, "jpg", baos1);
        Image image1 = Image.getInstance (baos1.toByteArray ());
        // （以左下角为原点）设置图片的坐标
        PdfPCell imageCell1 =  new PdfPCell();
        imageCell1.setImage (image1);
        imageCell1.setColspan (1);
        table.addCell (imageCell1);
        // 获取图片
        ByteArrayOutputStream baos2 = new ByteArrayOutputStream ();
        InputStream inputStream2 = new ClassPathResource ("template/2.png").getInputStream ();
        BufferedImage bufferedImage2 = ImageIO.read (inputStream2);
        ImageIO.write (bufferedImage2, "png", baos2);
        Image image2 = Image.getInstance (baos2.toByteArray ());
        // （以左下角为原点）设置图片的坐标
        PdfPCell imageCell2 =  new PdfPCell();
        imageCell2.setImage (image2);
        imageCell2.setColspan (1);
        table.addCell (imageCell2);
        // 获取图片
        ByteArrayOutputStream baos3 = new ByteArrayOutputStream ();
        InputStream inputStream3 = new ClassPathResource ("template/1.png").getInputStream ();
        BufferedImage bufferedImage3 = ImageIO.read (inputStream3);
        ImageIO.write (bufferedImage3, "png", baos3);
        Image image3 = Image.getInstance (baos1.toByteArray ());
        // （以左下角为原点）设置图片的坐标
        PdfPCell imageCell3 =  new PdfPCell();
        imageCell3.setImage (image3);
        imageCell3.setColspan (1);
        table.addCell (imageCell3);
        table.addCell(cellBuilder_28.buildCell("test111111111",BOLDFont));
        table.addCell(cellBuilder_28.buildCell("原币总额", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("100,000", NormalFont));
        table.addCell(cellBuilder_28.buildCell("本币总额", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("80,000", NormalFont));
        return table;
    }


}

