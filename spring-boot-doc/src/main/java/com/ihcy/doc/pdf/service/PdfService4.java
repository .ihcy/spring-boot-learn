package com.ihcy.doc.pdf.service;

import com.ihcy.doc.pdf.builder.CellBuilder;
import com.ihcy.doc.pdf.builder.FontBuilder;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import io.micrometer.core.instrument.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;


@Service
@Slf4j
public class PdfService4 {


    public void createPdf() {
        /**
         * 指定页面大小为A4，且自定义页边距(marginLeft、marginRight、marginTop、marginBottom)
         * Document document =new Document(PageSize.A4,50,50,30,20);
         */
        // 横版A4
        Document document = new Document(PageSize.A4);
        PdfWriter writer = null;
        try {
            // 创建PdfWriter对象
            writer = PdfWriter.getInstance(document, new FileOutputStream("test4.PDF"));
            document.addAuthor("杨波");
            document.addCreationDate();
            document.addKeywords("测试");
            document.addProducer();
            document.addCreator("www.ydc51.com");
            document.addTitle("测试标题");
            document.addSubject("测试PDF创建的主题");
            document.open();
            //添加一个内容段落 ,包含字体
            Paragraph titleParagraph = new Paragraph("收款单", FontBuilder.fontBuild(20, Font.BOLD));
            // 居中对齐
            titleParagraph.setAlignment(Element.ALIGN_CENTER);
            titleParagraph.setSpacingAfter(8);
            document.add(titleParagraph);

            // 表格
            document.add(getPdfPTable());
            document.add(getPdfPTable2());

            // 打印人，打印日期
            StringBuilder strb = new StringBuilder("录入人：");
            strb.append("杨波");
            strb.append("           审核人：");
            strb.append("研工部");
            Paragraph printParagraph = new Paragraph(strb.toString(), FontBuilder.fontBuild(11, Font.NORMAL));
            printParagraph.setSpacingBefore(3);
            printParagraph.setSpacingAfter(5);
            document.add(printParagraph);

            // 审批
            Paragraph auditParagraph = new Paragraph("审批情况", FontBuilder.fontBuild(20, Font.BOLD));
            // 居中对齐
            auditParagraph.setAlignment(Element.ALIGN_CENTER);
            auditParagraph.setSpacingAfter(8);
            document.add(auditParagraph);

            document.add(getAuditTable());

            // 打印人，打印日期
            StringBuilder crateString = new StringBuilder("制表人：");
            crateString.append("杨波");
            crateString.append("           制表日期：");
            crateString.append("2020-02-20");
            Paragraph createParagraph = new Paragraph(crateString.toString(), FontBuilder.fontBuild(11, Font.NORMAL));
            printParagraph.setSpacingBefore(8);
            printParagraph.setSpacingAfter(5);
            document.add(createParagraph);

        } catch (Exception e) {
            log.error("", e);
        } finally {
            // 关闭文档
            document.close();
            assert writer != null;
            writer.close();
        }

    }

    private PdfPTable getPdfPTable() throws DocumentException, IOException {
        // 7列的表.
        PdfPTable table = new PdfPTable(6);
        // 宽度100%填充
        table.setWidthPercentage(100);
        // 前间距
        table.setSpacingBefore(10f);
        // 后间距
        table.setSpacingAfter(5f);

        //设置列宽
        float[] columnWidths = {1f, 1f, 1f, 1f, 1f, 1f};
        table.setWidths(columnWidths);
        Font BOLDFont = FontBuilder.fontBuild(10, Font.BOLD);
        Font UNDERLINEFont = FontBuilder.fontBuild(10, Font.NORMAL);
        CellBuilder cellBuilder_28 = new CellBuilder(21);

        table.addCell(cellBuilder_28.buildCell(15, Element.ALIGN_RIGHT, "财务组织", BOLDFont));

        table.addCell(cellBuilder_28.buildCell(15, Element.ALIGN_LEFT, "上海慎则化工科技有限公司",
                UNDERLINEFont));

        table.addCell(cellBuilder_28.buildCell(15, Element.ALIGN_RIGHT, "单据号", BOLDFont));
        table.addCell(cellBuilder_28.buildCell(15, Element.ALIGN_LEFT, "323133232131313", UNDERLINEFont));
        table.addCell(cellBuilder_28.buildCell(15, Element.ALIGN_RIGHT, "单据日期", BOLDFont));
        table.addCell(cellBuilder_28.buildCell(15, Element.ALIGN_LEFT, "2020-01-14", UNDERLINEFont));
        table.addCell(cellBuilder_28.buildCell(15, Element.ALIGN_RIGHT, "收款账户", BOLDFont));
        table.addCell(cellBuilder_28.buildCell(12, Element.ALIGN_LEFT, "313122132132131123", UNDERLINEFont));

        table.addCell(cellBuilder_28.buildCell(15, Element.ALIGN_RIGHT, "结算方式", BOLDFont));
        table.addCell(cellBuilder_28.buildCell(12, Element.ALIGN_LEFT, "银行转账", UNDERLINEFont));
        table.addCell(cellBuilder_28.buildCell(15, Element.ALIGN_RIGHT, "币种", BOLDFont));
        table.addCell(cellBuilder_28.buildCell(12, Element.ALIGN_LEFT, "人民币", UNDERLINEFont));
        table.addCell(cellBuilder_28.buildCell(15, Element.ALIGN_RIGHT, "附件", BOLDFont));
        table.addCell(cellBuilder_28.buildCell(12, Element.ALIGN_LEFT, "0", UNDERLINEFont));

        table.addCell(cellBuilder_28.buildCell(15, Element.ALIGN_RIGHT, "本币汇率", BOLDFont));
        table.addCell(cellBuilder_28.buildCell(12, Element.ALIGN_LEFT, "1", UNDERLINEFont));
        table.addCell(cellBuilder_28.buildCell(15, Element.ALIGN_RIGHT,null, BOLDFont));
        table.addCell(cellBuilder_28.buildCell(15, Element.ALIGN_RIGHT,null, UNDERLINEFont));
        return table;
    }

    private PdfPTable getPdfPTable2() throws DocumentException, IOException {
        // 7列的表.
        PdfPTable table = new PdfPTable(6);
        // 宽度100%填充
        table.setWidthPercentage(100);
        // 后间距
        table.setSpacingAfter(5f);

        //设置列宽
        float[] columnWidths = {1f, 1f, 1f, 1f, 1f, 1f};
        table.setWidths(columnWidths);
        Font BOLDFont = FontBuilder.fontBuild(10, Font.BOLD);
        Font normalFont = FontBuilder.fontBuild(10, Font.NORMAL);
        CellBuilder cellBuilder_18 = new CellBuilder(18);

        table.addCell(cellBuilder_18.buildCell("往来对象", BOLDFont));
        table.addCell(cellBuilder_18.buildCell("客户名称", BOLDFont));
        table.addCell(cellBuilder_18.buildCell("部门", BOLDFont));
        table.addCell(cellBuilder_18.buildCell("业务员", BOLDFont));
        table.addCell(cellBuilder_18.buildCell("收支项目", BOLDFont));
        table.addCell(cellBuilder_18.buildCell("原币金额", BOLDFont));
        table.addCell(cellBuilder_18.buildCell("本币金额", BOLDFont));
        for (int i = 0; i < 4; i++) {
            table.addCell(cellBuilder_18.buildCell(null, normalFont));
            table.addCell(cellBuilder_18.buildCell(null, normalFont));
            table.addCell(cellBuilder_18.buildCell(null, normalFont));
            table.addCell(cellBuilder_18.buildCell(null, normalFont));
            table.addCell(cellBuilder_18.buildCell(null, normalFont));
            table.addCell(cellBuilder_18.buildCell(null, normalFont));
            table.addCell(cellBuilder_18.buildCell(null, normalFont));
        }
        return table;
    }


    private PdfPTable getAuditTable() throws DocumentException, IOException {
        // 8列的表.
        PdfPTable table = new PdfPTable(7);
        // 宽度100%填充
        table.setWidthPercentage(100);
        // 前间距
        table.setSpacingBefore(10f);
        // 后间距
        table.setSpacingAfter(5f);
        //设置列宽
        float[] columnWidths = {0.5f, 1f, 0.5f, 0.5f, 0.5f, 1f, 1f};
        table.setWidths(columnWidths);
        Font BOLDFont = FontBuilder.fontBuild(9, Font.BOLD);
        Font NormalFont = FontBuilder.fontBuild(9);
        CellBuilder cellBuilder_28 = new CellBuilder(19);

        table.addCell(cellBuilder_28.buildCell("单据类型", BOLDFont, Boolean.TRUE));

        table.addCell(cellBuilder_28.buildCell("付款",
                FontBuilder.fontBuild(11, Font.UNDERLINE), Boolean.TRUE));

        table.addCell(cellBuilder_28.buildCell("",
                FontBuilder.fontBuild(11), 2, Boolean.TRUE));

        table.addCell(cellBuilder_28.buildCell("单据编号", BOLDFont, Boolean.TRUE));

        table.addCell(cellBuilder_28.buildCell("1234567888",
                FontBuilder.fontBuild(11), Boolean.TRUE));

        table.addCell(cellBuilder_28.buildCell("",
                FontBuilder.fontBuild(11), 2, Boolean.TRUE));


        table.addCell(cellBuilder_28.buildCell("发送人", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("发送日期", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("审批人", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("审批状况", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("审批意见", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("审批日期", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("批语", BOLDFont));
        for (int i = 0; i < 4; i++) {
            table.addCell(cellBuilder_28.buildCell("发送人" + i, NormalFont));
            table.addCell(cellBuilder_28.buildCell("发送日期" + i, NormalFont));
            table.addCell(cellBuilder_28.buildCell("审批人" + i, NormalFont));
            table.addCell(cellBuilder_28.buildCell("审批状况" + i, NormalFont));
            table.addCell(cellBuilder_28.buildCell("审批意见" + i, NormalFont));
            table.addCell(cellBuilder_28.buildCell("审批日期" + i, NormalFont));
            table.addCell(cellBuilder_28.buildCell("批语" + i, NormalFont));

        }
        return table;
    }

    private Map<String, String> objectToMap(Object obj, String suffix) throws IllegalAccessException {
        Map<String, String> map = new HashMap<>();
        Class<?> clazz = obj.getClass();
        System.out.println(clazz);
        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            String fieldName = field.getName();
            if (StringUtils.isNotBlank(suffix)) {
                fieldName = fieldName + suffix;
            }
            Object value = field.get(obj);
            if (value != null) {
                map.put(fieldName, value.toString());
            }
        }
        return map;
    }

}

