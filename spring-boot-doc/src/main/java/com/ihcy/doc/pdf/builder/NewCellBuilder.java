package com.ihcy.doc.pdf.builder;

import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;

public class NewCellBuilder {

    private Integer fixHeight;

    public NewCellBuilder(Integer fixHeight) {
        this.fixHeight = fixHeight;
    }

    public NewCellBuilder() {
    }

    public PdfPCell buildCell() {
        PdfPCell cell = new PdfPCell ();
        if (fixHeight != null) {
            cell.setFixedHeight (fixHeight);
        }
        return cell;
    }

    /**
     * @param value 单元格内容
     * @param font  字体
     * @return
     */
    public PdfPCell buildCell(String value, Font font) {
        PdfPCell cell = buildCell ();
        cell.setPhrase (new Phrase (value, font));

        return cell;
    }

    /**
     * @param value  单元格内容
     * @param font   字体
     * @param border 边框样式
     * @return
     */
    public PdfPCell buildCell(String value, Font font, Integer border) {
        PdfPCell cell = buildCell ();
        cell.setPhrase (new Phrase (value, font));
        // 边框
        if (border != null) {
            cell.disableBorderSide (border);
        }
        return cell;
    }

    /**
     * @param value   单元格内容
     * @param font    字体
     * @param rowspan 跨行
     * @param colspan 跨列
     * @return
     */
    public PdfPCell buildCell(String value, Font font, Integer rowspan, Integer colspan) {
        PdfPCell cell = buildCell ();
        cell.setPhrase (new Phrase (value, font));
        // 跨行
        if (rowspan != null) {
            cell.setRowspan (rowspan);
        }
        // 跨列
        if (colspan != null) {
            cell.setColspan (colspan);
        }
        return cell;
    }

    /**
     * @param value   单元格内容
     * @param font    字体
     * @param rowspan 跨行
     * @param colspan 跨列
     * @param border  边框样式
     * @return
     */
    public PdfPCell buildCell(String value, Font font, Integer rowspan, Integer colspan, Integer border) {
        PdfPCell cell = buildCell ();
        cell.setPhrase (new Phrase (value, font));
        // 跨行
        if (rowspan != null) {
            cell.setRowspan (rowspan);
        }
        // 跨列
        if (colspan != null) {
            cell.setColspan (colspan);
        }
        // 边框
        if (border != null) {
            cell.disableBorderSide (border);
        }
        return cell;
    }

    /**
     * @param value           单元格内容
     * @param font            字体
     * @param rowspan         跨行
     * @param colspan         跨列
     * @param border          边框样式
     * @param horizontalAlign 水平
     * @return
     */
    public PdfPCell buildCell(String value, Font font, Integer rowspan, Integer colspan, Integer border, Integer horizontalAlign) {
        PdfPCell cell = buildCell ();
        cell.setPhrase (new Phrase (value, font));
        // 跨行
        if (rowspan != null) {
            cell.setRowspan (rowspan);
        }
        // 跨列
        if (colspan != null) {
            cell.setColspan (colspan);
        }
        // 边框
        if (border != null) {
            cell.disableBorderSide (border);
        }
        // 水平
        if (horizontalAlign != null) {
            cell.setHorizontalAlignment (horizontalAlign);
        }
        return cell;
    }


    /**
     * @param value           单元格内容
     * @param font            字体
     * @param rowspan         跨行
     * @param colspan         跨列
     * @param horizontalAlign 水平样式  ALIGN_CENTER
     * @param verticalAlign   垂直样式  ALIGN_MIDDLE
     * @param border          边框样式
     * @return
     */
    public PdfPCell buildCell(String value, Font font, Integer rowspan, Integer colspan, Integer border, Integer horizontalAlign, Integer verticalAlign) {
        PdfPCell cell = buildCell ();
        cell.setPhrase (new Phrase (value, font));
        // 跨行
        if (rowspan != null) {
            cell.setRowspan (rowspan);
        }
        // 跨列
        if (colspan != null) {
            cell.setColspan (colspan);
        }
        // 边框
        if (border != null) {
            cell.disableBorderSide (border);
        }
        // 水平
        if (horizontalAlign != null) {
            cell.setHorizontalAlignment (horizontalAlign);
        }
        // 垂直
        if (verticalAlign != null) {
            cell.setVerticalAlignment (verticalAlign);
        }


        return cell;
    }

    /**
     * 单元格中插入图片
     *
     * @param image
     * @param colspan
     * @param horizontalAlign 水平样式  ALIGN_CENTER
     * @return
     */
    public PdfPCell buildCell(Image image, Integer rowspan, Integer colspan, Integer border, Integer horizontalAlign) {
        PdfPCell cell = buildCell ();
        cell.setImage (image);
        if (rowspan != null) {
            cell.setRowspan (rowspan);
        }
        if (colspan != null) {
            cell.setColspan (colspan);
        }
        if (border != null) {
            cell.disableBorderSide (border);
        }
        // 水平
        if (horizontalAlign != null) {
            cell.setHorizontalAlignment (horizontalAlign);
        }
        return cell;
    }

}
