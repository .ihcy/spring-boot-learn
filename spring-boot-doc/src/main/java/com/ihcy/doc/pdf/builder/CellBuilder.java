package com.ihcy.doc.pdf.builder;

import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;

public class CellBuilder {

    private Integer fixHeight;

    public CellBuilder(Integer fixHeight) {
        this.fixHeight = fixHeight;
    }

    public CellBuilder() {
    }

    public PdfPCell buildCell() {
        PdfPCell cell = new PdfPCell ();
        if (fixHeight != null) {
            cell.setFixedHeight (fixHeight);
        }
        return cell;
    }

    /**
     * 创建单元格(指定 内容 字体)
     *
     * @param value
     * @param font
     */
    public PdfPCell buildCell(String value, Font font) {
        return buildCell (value, font, null, null, Element.ALIGN_MIDDLE, Element.ALIGN_CENTER, null);
    }

    /**
     * 创建单元格
     *
     * @param border 隐藏边框
     * @param align  水平对齐方式
     * @param value
     * @param font
     */
    public PdfPCell buildCell(int border, int align, String value, Font font) {
        PdfPCell cell = buildCell ();
        cell.setPhrase (new Phrase (value, font));
        cell.disableBorderSide (border);
        // 垂直对齐
        cell.setVerticalAlignment (Element.ALIGN_MIDDLE);
        // 水平对齐
        cell.setHorizontalAlignment (align);
        return cell;
    }

    /**
     * 跨单元格
     *
     * @param value
     * @param font
     * @param colspan
     * @return
     */
    public PdfPCell buildCell(String value, Font font, int colspan) {
        PdfPCell cell = buildCell (value, font);
        cell.setColspan (colspan);
        return cell;
    }

    /**
     * 是否无边框
     *
     * @param value
     * @param font
     * @param borderFlag 是-无边框
     */
    public PdfPCell buildCell(String value, Font font, boolean borderFlag) {
        PdfPCell cell = buildCell (value, font);
        if (borderFlag) {
            // 隐藏全部边框
            cell.disableBorderSide (15);
        }
        return cell;
    }

    /**
     * 跨单元格 无边框
     *
     * @param value
     * @param font
     * @param colspan    跨列单元格
     * @param borderFlag 是-无边框
     * @return
     */
    public PdfPCell buildCell(String value, Font font, int colspan, boolean borderFlag) {
        PdfPCell cell = buildCell (value, font, colspan);
        if (borderFlag) {
            // 隐藏全部边框
            cell.disableBorderSide (15);
        }
        cell.setPhrase (new Phrase (value, font));
        return cell;
    }

    /**
     * 无边框 对齐方式
     *
     * @param value
     * @param font
     * @param verticalAlign 垂直对齐
     * @param borderFlag    是-无边框
     * @return
     */
    public PdfPCell buildCell(String value, Font font, boolean borderFlag, int verticalAlign) {
        PdfPCell cell = buildCell ();
        cell.setPhrase (new Phrase (value, font));
        // 垂直对齐
        cell.setVerticalAlignment (verticalAlign);
        if (borderFlag) {
            // 隐藏全部边框
            cell.disableBorderSide (15);
        }
        return cell;
    }

    /**
     * 跨单元格无边框 对齐方式
     *
     * @param value
     * @param font
     * @param borderFlag    是-无边框
     * @param verticalAlign 垂直对齐
     * @param colspan       跨单元格
     * @return
     */
    public PdfPCell buildCell(String value, Font font, boolean borderFlag, int verticalAlign, int colspan) {
        PdfPCell cell = buildCell (value, font, borderFlag, verticalAlign);
        cell.setColspan (colspan);
        return cell;
    }

    /**
     * 跨单元格
     *
     * @param value   值
     * @param font    自提
     * @param colspan 跨行
     * @param rowspan 跨列
     * @return
     */
    public PdfPCell buildCell(String value, Font font, int colspan, int rowspan) {
        return buildCell (value, font, colspan, rowspan, null, null, null);
    }

    /**
     * @param value           单元格内容
     * @param font            字体
     * @param rowspan         跨行
     * @param colspan         跨列
     * @param verticalAlign   垂直样式
     * @param horizontalAlign 水平样式
     * @param border          边框样式
     * @return
     */
    public PdfPCell buildCell(String value, Font font, Integer colspan, Integer rowspan, Integer verticalAlign, Integer horizontalAlign, Integer border) {
        PdfPCell cell = buildCell ();
        cell.setPhrase (new Phrase (value, font));
        // 跨列
        if (colspan != null) {
            cell.setColspan (colspan);
        }
        // 跨行
        if (rowspan != null) {
            cell.setRowspan (rowspan);
        }
        // 垂直
        if (verticalAlign != null) {
            cell.setVerticalAlignment (verticalAlign);
        }
        // 水平
        if (horizontalAlign != null) {
            cell.setHorizontalAlignment (horizontalAlign);
        }
        // 边框
        if (border != null) {
            cell.setBorder (border);
        }
        return cell;
    }

    /**
     * 单元格中插入图片
     *
     * @param image
     * @param colspan
     * @return
     */
    public PdfPCell buildCell(Image image, int colspan, boolean borderFlag) {
        PdfPCell cell = buildCell ();
        cell.setImage (image);
        cell.setColspan (colspan);
        if (borderFlag) {
            // 隐藏全部边框
            cell.disableBorderSide (15);
        }
        return cell;
    }

}
