package com.ihcy.doc.pdf.service;

import com.ihcy.doc.pdf.builder.FontBuilder;
import com.ihcy.doc.pdf.builder.NewCellBuilder;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * 托书打印
 */
@Slf4j
@Service
public class TestPdfService {

    public void createPdf() {
        // 横版A4
        Document document = new Document (PageSize.A4, 50, 50, 30, 20);
        PdfWriter writer = null;
        try {
            // 创建PdfWriter对象
            writer = PdfWriter.getInstance (document, new FileOutputStream ("1.PDF"));
            document.addAuthor ("化亿达");
            document.addCreationDate ();
            document.addKeywords ("化亿达整箱订单托书");
            document.addProducer ();
            document.addCreator ("www.ydc51.com");
            document.addTitle ("测试标题");
            document.addSubject ("测试PDF创建的主题");
            document.open ();
            // 添加表格信息
            document.add (getPdfPTable ());
        } catch (Exception e) {
            log.error ("", e);
        } finally {
            // 关闭文档
            document.close ();
            assert writer != null;
            writer.close ();
        }
    }

    private PdfPTable getPdfPTable() throws DocumentException, IOException {
        // 7列的表.
        PdfPTable table = new PdfPTable (5);
        // 宽度100%填充
        table.setWidthPercentage (100);
        // 前间距
        table.setSpacingBefore (10f);
        // 后间距
        table.setSpacingAfter (5f);
        //设置列宽
        float[] columnWidths = {1f, 1.5f, 0.9f, 0.8f, 0.8f};
        table.setWidths (columnWidths);
        Font boldFont20 = FontBuilder.fontBuild (20, Font.BOLD);
        Font boldFont8 = FontBuilder.fontBuild (8, Font.BOLD);
        Font normalFont8 = FontBuilder.fontBuild (8);
        NewCellBuilder cellBuilder = new NewCellBuilder ();
        // 获取图片
        // 标题
        table.addCell (cellBuilder.buildCell (getImage ("template/3.png", "png"), 1, 1, 15, Element.ALIGN_LEFT));
        table.addCell (cellBuilder.buildCell ("SHIPPER ORDER", boldFont20, 1, 3, 15, Element.ALIGN_CENTER));
        table.addCell (cellBuilder.buildCell ("", boldFont20, 1, 3, 15, Element.ALIGN_CENTER));

        // 业务编号 制表日期
        for (int i = 0; i < 60; i++) {
            table.addCell (cellBuilder.buildCell ("业务编号:HSDF20200806000001", normalFont8, 1, 5));
        }
        return table;
    }

    private Image getImage(String uri, String suffix) throws IOException, BadElementException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream ();
        InputStream inputStream = new ClassPathResource (uri).getInputStream ();
        BufferedImage bufferedImage = ImageIO.read (inputStream);
        ImageIO.write (bufferedImage, suffix, baos);
        return Image.getInstance (baos.toByteArray ());
    }
}
