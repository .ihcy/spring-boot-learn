package com.ihcy.doc.pdf.service;

import com.ihcy.doc.pdf.builder.FontBuilder;
import com.ihcy.doc.pdf.builder.NewCellBuilder;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * 托书打印
 */
@Slf4j
@Service
public class InnerPdfService {

    public void createPdf() {
        // 横版A4
        Document document = new Document (PageSize.A4, 50, 50, 15, 15);
        PdfWriter writer = null;
        try {
            // 创建PdfWriter对象
            writer = PdfWriter.getInstance (document, new FileOutputStream ("内装通知书.PDF"));
            document.addAuthor ("化亿达");
            document.addCreationDate ();
            document.addKeywords ("化亿达内装通知书");
            document.addProducer ();
            document.addCreator ("www.ehcem56.com");
            document.addTitle ("整箱订单");
            document.addSubject ("内装通知书");
            document.open ();
            // 添加表格信息
            document.add (getPdfPTable ());
        } catch (Exception e) {
            log.error ("", e);
        } finally {
            // 关闭文档
            document.close ();
            assert writer != null;
            writer.close ();
        }
    }

    private PdfPTable getPdfPTable() throws DocumentException, IOException {
        // 7列的表.
        PdfPTable table = new PdfPTable (8);
        // 宽度100%填充
        table.setWidthPercentage (100);
        // 前间距
        table.setSpacingBefore (10f);
        // 后间距
        table.setSpacingAfter (5f);

        float[] columnWidths = {1.60f, 2f, 0.83f, 1.17f, 1.6f, 1.33f, 1f, 0.83f};
        table.setWidths (columnWidths);
        Font boldFont20 = FontBuilder.fontBuild (20, Font.BOLD);
        Font boldFont10 = FontBuilder.fontBuild (10, Font.BOLD);
        Font normalFont10 = FontBuilder.fontBuild (10);
        Font normalFont8 = FontBuilder.fontBuild (8);
        NewCellBuilder cellBuilder20 = new NewCellBuilder (20);
        NewCellBuilder cellBuilder40 = new NewCellBuilder (40);
        // 获取图片
        // 标题
        table.addCell (cellBuilder40.buildCell (getImage ("template/3.png", "png"), 1, 2, 15, Element.ALIGN_LEFT));
        table.addCell (cellBuilder40.buildCell ("内装通知书", boldFont20, 1, 3, 15, Element.ALIGN_CENTER));
        table.addCell (cellBuilder40.buildCell (" ", boldFont20, 1, 3, 15, Element.ALIGN_CENTER));
        // 第一行
        table.addCell (cellBuilder20.buildCell ("致:", normalFont10, 1, 1, 15, Element.ALIGN_CENTER));
        table.addCell (cellBuilder20.buildCell ("内装仓库", boldFont10, 1, 3, 15, Element.ALIGN_LEFT));
        table.addCell (cellBuilder20.buildCell ("联系人:", normalFont10, 1, 1, 15, Element.ALIGN_CENTER));
        table.addCell (cellBuilder20.buildCell ("张勇", normalFont10, 1, 3, 15, Element.ALIGN_LEFT));
        // 2
        BaseColor baseColor = new BaseColor (0, 173, 199);
        PdfPCell cell1 = cellBuilder20.buildCell ("一、配舱信息", boldFont10, 1, 8, null, Element.ALIGN_LEFT);
        cell1.setBackgroundColor (baseColor);
        table.addCell (cell1);
        // 3
        table.addCell (cellBuilder20.buildCell ("提单号：", normalFont10, 1, 1, null, Element.ALIGN_CENTER));
        table.addCell (cellBuilder20.buildCell ("TIDAN20200805001", normalFont10, 1, 3, null, Element.ALIGN_LEFT));
        table.addCell (cellBuilder20.buildCell ("开航日期:", normalFont10, 1, 1, null, Element.ALIGN_CENTER));
        table.addCell (cellBuilder20.buildCell ("2020-08-05", normalFont10, 1, 3, null, Element.ALIGN_LEFT));

        // 4
        table.addCell (cellBuilder20.buildCell ("船名：", normalFont10, 1, 1, null, Element.ALIGN_CENTER));
        table.addCell (cellBuilder20.buildCell ("山东舰", normalFont10, 1, 3, null, Element.ALIGN_LEFT));
        table.addCell (cellBuilder20.buildCell ("航次:", normalFont10, 1, 1, null, Element.ALIGN_CENTER));
        table.addCell (cellBuilder20.buildCell ("第1次航行", normalFont10, 1, 3, null, Element.ALIGN_LEFT));

        // 5
        table.addCell (cellBuilder20.buildCell ("起运港：", normalFont10, 1, 1, null, Element.ALIGN_CENTER));
        table.addCell (cellBuilder20.buildCell ("海南三亚", normalFont10, 1, 3, null, Element.ALIGN_LEFT));
        table.addCell (cellBuilder20.buildCell ("目的港:", normalFont10, 1, 1, null, Element.ALIGN_CENTER));
        table.addCell (cellBuilder20.buildCell ("吉布提", normalFont10, 1, 3, null, Element.ALIGN_LEFT));

        // 6
        table.addCell (cellBuilder20.buildCell ("箱型箱量：", normalFont10, 1, 1, null, Element.ALIGN_CENTER));
        table.addCell (cellBuilder20.buildCell ("20GP*1;40GP*2", normalFont10, 1, 7, null, Element.ALIGN_LEFT));
        // 7
        table.addCell (cellBuilder20.buildCell ("开港日期：", normalFont10, 1, 1, null, Element.ALIGN_CENTER));
        table.addCell (cellBuilder20.buildCell ("2020-08-05", normalFont10, 1, 3, null, Element.ALIGN_LEFT));
        table.addCell (cellBuilder20.buildCell ("截港日期:", normalFont10, 1, 1, null, Element.ALIGN_CENTER));
        table.addCell (cellBuilder20.buildCell ("2020-09-05", normalFont10, 1, 3, null, Element.ALIGN_LEFT));

        // 8
        PdfPCell cell2 = cellBuilder20.buildCell ("二、货物内装信息", boldFont10, 1, 8, null, Element.ALIGN_LEFT);
        cell2.setBackgroundColor (baseColor);
        table.addCell (cell2);

        // 9
        table.addCell (cellBuilder20.buildCell ("进仓编号：", normalFont10, 1, 1, null, Element.ALIGN_CENTER));
        table.addCell (cellBuilder20.buildCell ("JIN20200800001", normalFont10, 1, 3, null, Element.ALIGN_LEFT));
        table.addCell (cellBuilder20.buildCell ("", normalFont10, 1, 1, null, Element.ALIGN_CENTER));
        table.addCell (cellBuilder20.buildCell ("", normalFont10, 1, 3, null, Element.ALIGN_LEFT));

        // 10
        addFourCell (table, cellBuilder20, normalFont10, "预计入库日期:", "2020-08-05", "通知日期:", "2020-08-10");
        // 11
        addFourCell (table, cellBuilder20, normalFont10, "放单日期:", "2020-08-05", "提箱日期:", "2020-08-10");
        // 12
        table.addCell (cellBuilder20.buildCell ("加固要求：", normalFont10, 1, 1, null, Element.ALIGN_CENTER));
        table.addCell (cellBuilder20.buildCell ("xxxxxxxxxxxxxxxxx", normalFont10, 1, 7, null, Element.ALIGN_LEFT));
        // 13
        table.addCell (cellBuilder20.buildCell ("备注：", normalFont10, 1, 1, null, Element.ALIGN_CENTER));
        table.addCell (cellBuilder20.buildCell ("xxxxxxxxxxxxxxxxx", normalFont10, 1, 7, null, Element.ALIGN_LEFT));

        // 14
        PdfPCell cell3 = cellBuilder20.buildCell ("三、货物信息", boldFont10, 1, 8, null, Element.ALIGN_LEFT);
        cell3.setBackgroundColor (baseColor);
        table.addCell (cell3);
        // 15
        table.addCell (cellBuilder20.buildCell ("英文品名", normalFont10, 1, 2, null, Element.ALIGN_CENTER));
        table.addCell (cellBuilder20.buildCell ("唛头", normalFont10, 1, 1, null, Element.ALIGN_CENTER));
        table.addCell (cellBuilder20.buildCell ("批次", normalFont10, 1, 1, null, Element.ALIGN_CENTER));
        table.addCell (cellBuilder20.buildCell ("数量", normalFont10, 1, 1, null, Element.ALIGN_CENTER));
        table.addCell (cellBuilder20.buildCell ("包装", normalFont10, 1, 1, null, Element.ALIGN_CENTER));
        table.addCell (cellBuilder20.buildCell ("毛重KGS", normalFont10, 1, 1, null, Element.ALIGN_CENTER));
        table.addCell (cellBuilder20.buildCell ("立方数CBM", normalFont10, 1, 1, null, Element.ALIGN_CENTER));
        // 14 行货物满
        // 不满14 补足 14
        // 14- 22 补足 22
        for (int i = 0; i < 22; i++) {
            table.addCell (cellBuilder20.buildCell (String.format ("goods %d", i), normalFont10, 1, 2, 11, Element.ALIGN_CENTER));
            table.addCell (cellBuilder20.buildCell (String.format ("唛头 %d", i), normalFont10, 1, 1, 15, Element.ALIGN_CENTER));
            table.addCell (cellBuilder20.buildCell (String.format ("批次 %d", i), normalFont10, 1, 1, 15, Element.ALIGN_CENTER));
            table.addCell (cellBuilder20.buildCell (String.format ("数量 %d", i), normalFont10, 1, 1, 15, Element.ALIGN_CENTER));
            table.addCell (cellBuilder20.buildCell (String.format ("包装 %d", i), normalFont10, 1, 1, 15, Element.ALIGN_CENTER));
            table.addCell (cellBuilder20.buildCell (String.format ("毛重KGS %d", i), normalFont10, 1, 1, 15, Element.ALIGN_CENTER));
            table.addCell (cellBuilder20.buildCell (String.format ("立方数CBM %d", i), normalFont10, 1, 1, 7, Element.ALIGN_CENTER));
        }
        // 26
        table.addCell (cellBuilder20.buildCell ("合计：", normalFont10, 1, 2, 8, Element.ALIGN_RIGHT));
        table.addCell (cellBuilder20.buildCell ("10 件", normalFont10, 1, 2, 12, Element.ALIGN_CENTER));
        table.addCell (cellBuilder20.buildCell (" ", normalFont10, 1, 2, 12, Element.ALIGN_RIGHT));
        table.addCell (cellBuilder20.buildCell ("xxKSG", normalFont10, 1, 1, 12, Element.ALIGN_RIGHT));
        table.addCell (cellBuilder20.buildCell ("xxCBM", normalFont10, 1, 1, 4, Element.ALIGN_RIGHT));

        // 27
        PdfPCell cell4 = cellBuilder20.buildCell ("四、联系信息", boldFont10, 1, 8, null, Element.ALIGN_LEFT);
        cell4.setBackgroundColor (baseColor);
        table.addCell (cell4);
        // 28-30
        table.addCell (cellBuilder20.buildCell ("客服：", normalFont10, 1, 1, 11, Element.ALIGN_CENTER));
        table.addCell (cellBuilder20.buildCell ("张勇", normalFont10, 1, 2, 15, Element.ALIGN_LEFT));
        table.addCell (cellBuilder20.buildCell ("邮箱：", normalFont10, 1, 1, 15, Element.ALIGN_CENTER));
        table.addCell (cellBuilder20.buildCell ("service@echem56.com", normalFont10, 1, 4, 7, Element.ALIGN_LEFT));
        table.addCell (cellBuilder20.buildCell ("地址：", normalFont10, 1, 1, 11, Element.ALIGN_CENTER));
        table.addCell (cellBuilder20.buildCell ("上海市浦东新区锦绣东路2777弄锦绣申江39号楼", normalFont10, 1, 7, 7, Element.ALIGN_LEFT));
        //31
        PdfPCell cell5 = cellBuilder20.buildCell ("五、危险品装箱注意事项：", boldFont10, 1, 8, null, Element.ALIGN_LEFT);
        cell5.setBackgroundColor (baseColor);
        table.addCell (cell5);
        // 32
        table.addCell (cellBuilder20.buildCell ("1、请务必确保送仓的货物与委托的货物完全相同。如有任何不同，请立即联系客服人员。如因此发生货物误发，漏发，额外分货等一切风险和责任由仓库承担。",
                normalFont8, 1, 5, 11));
        table.addCell (cellBuilder20.buildCell (getImage ("template/2.png", "png"), 4, 3, 5, Element.ALIGN_RIGHT));
        // 33-35
        table.addCell (cellBuilder20.buildCell ("2、危险品要求每件货物上均显示CLASS、IMO 危标、UN NO.和PROPER SHIPPING NAME，并贴上完整清楚的货物唛头；请注意必须显示品名含量的百分比(和报关一致)，以防止海关查验时发生扣货等问题。"
                , normalFont8, 1, 5, 11));
        table.addCell (cellBuilder20.buildCell ("3、如危险品货物目的地为欧盟，客户到货后请查看标签，是否符合CLP法规的要求。若未按CLP法规执行，请提前告知相关客服人员。"
                , normalFont8, 1, 5, 11));
        table.addCell (cellBuilder20.buildCell ("4、要求危险品货物，出运时必须打托缠膜，并做好加固，托盘必须使用实木熏蒸(带章)/免熏蒸/塑料托盘。货物外包装上必须有与危包证/船级证/钢瓶证一致的标记唛码。"
                , normalFont8, 1, 5, 9));
        return table;
    }

    private void addFourCell(PdfPTable table, NewCellBuilder builder, Font font, String str1, String str2, String str3, String str4) {
        table.addCell (builder.buildCell (str1, font, 1, 1, null, Element.ALIGN_CENTER));
        table.addCell (builder.buildCell (str2, font, 1, 3, null, Element.ALIGN_LEFT));
        table.addCell (builder.buildCell (str3, font, 1, 1, null, Element.ALIGN_CENTER));
        table.addCell (builder.buildCell (str4, font, 1, 3, null, Element.ALIGN_LEFT));
    }

    private Image getImage(String uri, String suffix) throws IOException, BadElementException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream ();
        InputStream inputStream = new ClassPathResource (uri).getInputStream ();
        BufferedImage bufferedImage = ImageIO.read (inputStream);
        ImageIO.write (bufferedImage, suffix, baos);
        return Image.getInstance (baos.toByteArray ());
    }
}
