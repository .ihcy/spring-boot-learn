package com.ihcy.doc.pdf.service;

import com.ihcy.doc.pdf.builder.FontBuilder;
import com.ihcy.doc.pdf.builder.NewCellBuilder;
import com.ihcy.doc.vo.Container;
import com.ihcy.doc.vo.ContainerItem;
import com.ihcy.doc.vo.EntrustContainerUtil;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 托书打印
 */
@Slf4j
@Service
public class EntrustPdfService {

    public void createPdf(List<Container> containers, String name) {
        // 横版A4
        Document document = new Document (PageSize.A4, 50, 50, 15, 15);
        PdfWriter writer = null;
        try {
            // 创建PdfWriter对象
            writer = PdfWriter.getInstance (document, new FileOutputStream ("托书" + name + ".PDF"));
            document.addAuthor ("化亿达");
            document.addCreationDate ();
            document.addKeywords ("化亿达整箱订单托书");
            document.addProducer ();
            document.addCreator ("www.ydc51.com");
            document.addTitle ("测试标题");
            document.addSubject ("测试PDF创建的主题");
            document.open ();
            // 添加表格信息
            document.add (getPdfPTable (containers));
        } catch (Exception e) {
            e.printStackTrace ();
            log.error (e.getMessage ());
        } finally {
            // 关闭文档
            document.close ();
            assert writer != null;
            writer.close ();
        }
    }

    private PdfPTable getPdfPTable(List<Container> containers) throws DocumentException, IOException {
        // 7列的表.
        PdfPTable table = new PdfPTable (5);
        // 宽度100%填充
        table.setWidthPercentage (100);
        // 前间距
        table.setSpacingBefore (10f);
        // 后间距
        table.setSpacingAfter (5f);

        //设置列宽
        float[] columnWidths = {1f, 1.5f, 0.9f, 0.8f, 0.8f};
        table.setWidths (columnWidths);
        Font boldFont20 = FontBuilder.fontBuild (20, Font.BOLD);
        Font boldFont8 = FontBuilder.fontBuild (8, Font.BOLD);
        Font normalFont8 = FontBuilder.fontBuild (8);
        Font blueFont8 = FontBuilder.fontBuild (8, BaseColor.BLUE, Font.UNDERLINE);
        NewCellBuilder cellBuilder = new NewCellBuilder ();
        NewCellBuilder cellBuilder15 = new NewCellBuilder (30);
        // 获取图片
        // 标题
        table.addCell (cellBuilder.buildCell (getImage ("template/3.png", "png"), 1, 1, 15, Element.ALIGN_LEFT));
        table.addCell (cellBuilder.buildCell ("SHIPPER ORDER", boldFont20, 1, 3, 15, Element.ALIGN_CENTER));
        table.addCell (cellBuilder.buildCell (" ", boldFont20, 1, 3, 15, Element.ALIGN_CENTER));
        // 共60行
        // 业务编号 制表日期 1-3
        table.addCell (cellBuilder.buildCell ("业务编号:HSDF20200806000001", boldFont8, 1, 2, 15, Element.ALIGN_LEFT));
        table.addCell (cellBuilder.buildCell (" ", boldFont8, 1, 2, 15, Element.ALIGN_LEFT));
        table.addCell (cellBuilder.buildCell ("制表日期: 2020-08-05", normalFont8, 1, 1, 15, Element.ALIGN_RIGHT));
        // 4 行
        table.addCell (cellBuilder.buildCell ("Shipper 发货人  张勇", normalFont8, 1, 2, 10, Element.ALIGN_LEFT));
        table.addCell (cellBuilder.buildCell ("To 张三", normalFont8, 1, 2, 10, Element.ALIGN_LEFT));
        table.addCell (cellBuilder.buildCell ("联系人 李斯", normalFont8, 1, 1, 6, Element.ALIGN_LEFT));
        // 发货人详情 5-10 行
        for (int i = 0; i < 6; i++) {
            table.addCell (cellBuilder.buildCell ("发货人" + i, normalFont8, 1, 2, 11, Element.ALIGN_LEFT));
            if (i == 4) {
                // 船公司
                table.addCell (cellBuilder.buildCell ("船公司: 中国外运长航有限集团", normalFont8, 1, 3, 3, Element.ALIGN_LEFT));
            } else if (i == 5) {
                table.addCell (cellBuilder.buildCell ("航线: 上海 -> 东京", normalFont8, 1, 3, 3, Element.ALIGN_LEFT));
            } else {
                table.addCell (cellBuilder.buildCell (" ", normalFont8, 1, 3, 3, Element.ALIGN_LEFT));
            }

        }

        // 收货人 11 行
        table.addCell (cellBuilder.buildCell ("Consignee 收货人  李勇", normalFont8, 1, 2, 10, Element.ALIGN_LEFT));
        table.addCell (cellBuilder.buildCell ("运费：RMB 1000", normalFont8, 1, 3, 3, Element.ALIGN_LEFT));

        // 收货人 详情 12-17 行
        for (int i = 0; i < 6; i++) {
            table.addCell (cellBuilder.buildCell ("收货人" + i, normalFont8, 1, 2, 11, Element.ALIGN_LEFT));
            table.addCell (cellBuilder.buildCell (" ", normalFont8, 1, 3, 3, Element.ALIGN_LEFT));
        }
        // 通知人 18行
        table.addCell (cellBuilder.buildCell ("Notify Parity 通知人  赵勇", normalFont8, 1, 2, 10, Element.ALIGN_LEFT));
        table.addCell (cellBuilder.buildCell ("其他要求：", normalFont8, 1, 3, 3, Element.ALIGN_LEFT));

        // 通知人 19-24
        List<String> notifyList = Arrays.asList ("fffffffffffffff", "gggggggggggggggg");
        for (int i = 0; i < 6; i++) {
            table.addCell (cellBuilder.buildCell ("通知人" + i, normalFont8, 1, 2, 11, Element.ALIGN_LEFT));
            if (i == 0) {
                table.addCell (cellBuilder.buildCell ("占据 8 行，表单一共60行", normalFont8, 8, 3, 1));
            }

        }
        // Place of receipt & Port of loading 起运港 25-26行
        table.addCell (cellBuilder.buildCell ("Place of receipt 25行", normalFont8, 1, 1, 2, Element.ALIGN_CENTER));
        table.addCell (cellBuilder.buildCell ("Port of loading 起运港", normalFont8, 1, 1, 2, Element.ALIGN_CENTER));
        table.addCell (cellBuilder.buildCell ("上海", normalFont8, 1, 1, 1, Element.ALIGN_CENTER));
        table.addCell (cellBuilder.buildCell ("东京", normalFont8, 1, 1, 1, Element.ALIGN_CENTER));

        // Ocean Vessel       Voy NO. 27-28行
        table.addCell (cellBuilder.buildCell ("Ocean Vessel Voy NO.", normalFont8, 1, 1, 2, Element.ALIGN_CENTER));
        table.addCell (cellBuilder.buildCell ("Port of discharge 卸货港", normalFont8, 1, 1, 2, Element.ALIGN_CENTER));
        table.addCell (cellBuilder.buildCell ("Final destination 目的港", normalFont8, 1, 1, 2, Element.ALIGN_CENTER));
        table.addCell (cellBuilder.buildCell ("开航日期", normalFont8, 1, 2, 2, Element.ALIGN_CENTER));
        table.addCell (cellBuilder.buildCell ("不知道填啥", normalFont8, 1, 1, 1, Element.ALIGN_CENTER));
        table.addCell (cellBuilder.buildCell ("上海卸货港", normalFont8, 1, 1, 1, Element.ALIGN_CENTER));
        table.addCell (cellBuilder.buildCell ("东西目的港", normalFont8, 1, 1, 1, Element.ALIGN_CENTER));
        table.addCell (cellBuilder.buildCell ("2020-08-05", normalFont8, 1, 2, 1, Element.ALIGN_CENTER));

        // 货物信息 29-30行
        table.addCell (cellBuilder.buildCell ("Marks & Nos.", normalFont8, 1, 1, 2, Element.ALIGN_CENTER));
        table.addCell (cellBuilder.buildCell ("Description of Goods", normalFont8, 1, 1, 2, Element.ALIGN_CENTER));
        table.addCell (cellBuilder.buildCell ("Quantity/Package", normalFont8, 1, 1, 2, Element.ALIGN_CENTER));
        table.addCell (cellBuilder.buildCell ("Gross Weight", normalFont8, 1, 1, 2, Element.ALIGN_CENTER));
        table.addCell (cellBuilder.buildCell ("Measurement", normalFont8, 1, 1, 2, Element.ALIGN_CENTER));
        table.addCell (cellBuilder.buildCell (" 唛头 30行", normalFont8, 1, 1, 1, Element.ALIGN_CENTER));
        table.addCell (cellBuilder.buildCell ("货物信息", normalFont8, 1, 1, 1, Element.ALIGN_CENTER));
        table.addCell (cellBuilder.buildCell ("数量/包装", normalFont8, 1, 1, 1, Element.ALIGN_CENTER));
        table.addCell (cellBuilder.buildCell ("毛重", normalFont8, 1, 1, 1, Element.ALIGN_CENTER));
        table.addCell (cellBuilder.buildCell ("立方数", normalFont8, 1, 1, 1, Element.ALIGN_CENTER));
        // 26 行 满页 ;  分页至少33 行，26-33 行 补满行，第33行 加下划线

        List<Container> dangerContainers = new ArrayList<> ();
        List<ContainerItem> commonItems = new ArrayList<> ();
        EntrustContainerUtil.getContainerKind (containers, dangerContainers, commonItems);
        int row = 0;
        // 普货
        for (ContainerItem commonItem : commonItems) {
            row++;
            if (row == 1) {
                table.addCell (cellBuilder.buildCell ("货物类型：普货", boldFont8, 1, 1, 3, Element.ALIGN_CENTER));
            } else if (row == 35) {
                table.addCell (cellBuilder.buildCell ("  ", boldFont8, 1, 1, 1, Element.ALIGN_CENTER));
            } else if (row == 36) {
                table.addCell (cellBuilder.buildCell ("  ", boldFont8, 1, 1, 2, Element.ALIGN_CENTER));
            } else {
                table.addCell (cellBuilder.buildCell ("  ", boldFont8, 1, 1, 3, Element.ALIGN_CENTER));
            }
            if (row == 35) {
                table.addCell (cellBuilder.buildCell (commonItem.getItemNameEn (), boldFont8, 1, 1, 1, Element.ALIGN_CENTER));
                table.addCell (cellBuilder.buildCell (commonItem.getQuantity () + "/包装", boldFont8, 1, 1, 1, Element.ALIGN_CENTER));
                table.addCell (cellBuilder.buildCell (commonItem.getGrossWeight ().stripTrailingZeros ().toPlainString (), boldFont8, 1, 1, 1, Element.ALIGN_CENTER));
                table.addCell (cellBuilder.buildCell (commonItem.getVolume ().stripTrailingZeros ().toPlainString (), boldFont8, 1, 1, 1, Element.ALIGN_CENTER));
            } else if (row == 36) {
                table.addCell (cellBuilder.buildCell (commonItem.getItemNameEn (), boldFont8, 1, 1, 2, Element.ALIGN_CENTER));
                table.addCell (cellBuilder.buildCell (commonItem.getQuantity () + "/包装", boldFont8, 1, 1, 2, Element.ALIGN_CENTER));
                table.addCell (cellBuilder.buildCell (commonItem.getGrossWeight ().stripTrailingZeros ().toPlainString (), boldFont8, 1, 1, 2, Element.ALIGN_CENTER));
                table.addCell (cellBuilder.buildCell (commonItem.getVolume ().stripTrailingZeros ().toPlainString (), boldFont8, 1, 1, 2, Element.ALIGN_CENTER));
            } else {
                table.addCell (cellBuilder.buildCell (commonItem.getItemNameEn (), boldFont8, 1, 1, 3, Element.ALIGN_CENTER));
                table.addCell (cellBuilder.buildCell (commonItem.getQuantity () + "/包装", boldFont8, 1, 1, 3, Element.ALIGN_CENTER));
                table.addCell (cellBuilder.buildCell (commonItem.getGrossWeight ().stripTrailingZeros ().toPlainString (), boldFont8, 1, 1, 3, Element.ALIGN_CENTER));
                table.addCell (cellBuilder.buildCell (commonItem.getVolume ().stripTrailingZeros ().toPlainString (), boldFont8, 1, 1, 3, Element.ALIGN_CENTER));
            }
        }
        // 危险品
        for (int i = 0; i < dangerContainers.size (); i++) {
            row++;
            if (i == 0) {
                table.addCell (cellBuilder.buildCell ("货物类型：危险品", boldFont8, 1, 1, 3, Element.ALIGN_CENTER));
            } else if (row == 35) {
                table.addCell (cellBuilder.buildCell ("   " + row, boldFont8, 1, 1, 1, Element.ALIGN_CENTER));
            } else if (row == 36) {
                table.addCell (cellBuilder.buildCell ("   " + row, boldFont8, 1, 1, 2, Element.ALIGN_CENTER));
            } else {
                table.addCell (cellBuilder.buildCell ("   " + row, boldFont8, 1, 1, 3, Element.ALIGN_CENTER));
            }
            table.addCell (cellBuilder.buildCell (dangerContainers.get (i).getCtnType (), boldFont8, 1, 1, 3, Element.ALIGN_LEFT));
            table.addCell (cellBuilder.buildCell ("   " + row, boldFont8, 1, 1, 3, Element.ALIGN_CENTER));
            table.addCell (cellBuilder.buildCell ("   " + row, boldFont8, 1, 1, 3, Element.ALIGN_CENTER));
            table.addCell (cellBuilder.buildCell ("   " + row, boldFont8, 1, 1, 3, Element.ALIGN_CENTER));
            for (ContainerItem dangerItem : dangerContainers.get (i).getItemList ()) {
                row++;
                if (row == 35) {
                    table.addCell (cellBuilder.buildCell ("   " + row, boldFont8, 1, 1, 1, Element.ALIGN_CENTER));
                    table.addCell (cellBuilder.buildCell (dangerItem.getItemNameEn (), boldFont8, 1, 1, 1, Element.ALIGN_CENTER));
                    table.addCell (cellBuilder.buildCell ("   " + row, boldFont8, 1, 1, 1, Element.ALIGN_CENTER));
                    table.addCell (cellBuilder.buildCell ("   " + row, boldFont8, 1, 1, 1, Element.ALIGN_CENTER));
                    table.addCell (cellBuilder.buildCell ("   " + row, boldFont8, 1, 1, 1, Element.ALIGN_CENTER));
                } else if (row == 36) {
                    table.addCell (cellBuilder.buildCell ("   " + row, boldFont8, 1, 1, 2, Element.ALIGN_CENTER));
                    table.addCell (cellBuilder.buildCell (dangerItem.getItemNameEn (), boldFont8, 1, 1, 2, Element.ALIGN_CENTER));
                    table.addCell (cellBuilder.buildCell ("   " + row, boldFont8, 1, 1, 2, Element.ALIGN_CENTER));
                    table.addCell (cellBuilder.buildCell ("   " + row, boldFont8, 1, 1, 2, Element.ALIGN_CENTER));
                    table.addCell (cellBuilder.buildCell ("   " + row, boldFont8, 1, 1, 2, Element.ALIGN_CENTER));
                } else {
                    table.addCell (cellBuilder.buildCell ("   " + row, boldFont8, 1, 1, 3, Element.ALIGN_CENTER));
                    table.addCell (cellBuilder.buildCell (dangerItem.getItemNameEn (), boldFont8, 1, 1, 3, Element.ALIGN_CENTER));
                    table.addCell (cellBuilder.buildCell ("   " + row, boldFont8, 1, 1, 3, Element.ALIGN_CENTER));
                    table.addCell (cellBuilder.buildCell ("   " + row, boldFont8, 1, 1, 3, Element.ALIGN_CENTER));
                    table.addCell (cellBuilder.buildCell ("   " + row, boldFont8, 1, 1, 3, Element.ALIGN_CENTER));
                }
                row++;
                if (row == 35) {
                    table.addCell (cellBuilder.buildCell ("   " + row, boldFont8, 1, 1, 1, Element.ALIGN_CENTER));
                    table.addCell (cellBuilder.buildCell ("UN NO:" + dangerItem.getUnno () + " CLASS NO: " + dangerItem.getClassNo (), boldFont8, 1, 1, 1, Element.ALIGN_CENTER));
                    table.addCell (cellBuilder.buildCell (dangerItem.getQuantity () + "/包装", boldFont8, 1, 1, 1, Element.ALIGN_CENTER));
                    table.addCell (cellBuilder.buildCell (dangerItem.getGrossWeight ().stripTrailingZeros ().toPlainString (), boldFont8, 1, 1, 1, Element.ALIGN_CENTER));
                    table.addCell (cellBuilder.buildCell (dangerItem.getVolume ().stripTrailingZeros ().toPlainString (), boldFont8, 1, 1, 1, Element.ALIGN_CENTER));
                } else if (row == 35) {
                    table.addCell (cellBuilder.buildCell ("   " + row, boldFont8, 1, 1, 2, Element.ALIGN_CENTER));
                    table.addCell (cellBuilder.buildCell ("UN NO:" + dangerItem.getUnno () + " CLASS NO: " + dangerItem.getClassNo (), boldFont8, 1, 1, 2, Element.ALIGN_CENTER));
                    table.addCell (cellBuilder.buildCell (dangerItem.getQuantity () + "/包装", boldFont8, 1, 1, 2, Element.ALIGN_CENTER));
                    table.addCell (cellBuilder.buildCell (dangerItem.getGrossWeight ().stripTrailingZeros ().toPlainString (), boldFont8, 1, 1, 2, Element.ALIGN_CENTER));
                    table.addCell (cellBuilder.buildCell (dangerItem.getVolume ().stripTrailingZeros ().toPlainString (), boldFont8, 1, 1, 2, Element.ALIGN_CENTER));
                } else {
                    table.addCell (cellBuilder.buildCell ("   " + row, boldFont8, 1, 1, 3, Element.ALIGN_CENTER));
                    table.addCell (cellBuilder.buildCell ("UN NO:" + dangerItem.getUnno () + " CLASS NO: " + dangerItem.getClassNo (), boldFont8, 1, 1, 3, Element.ALIGN_CENTER));
                    table.addCell (cellBuilder.buildCell (dangerItem.getQuantity () + "/包装", boldFont8, 1, 1, 3, Element.ALIGN_CENTER));
                    table.addCell (cellBuilder.buildCell (dangerItem.getGrossWeight ().stripTrailingZeros ().toPlainString (), boldFont8, 1, 1, 3, Element.ALIGN_CENTER));
                    table.addCell (cellBuilder.buildCell (dangerItem.getVolume ().stripTrailingZeros ().toPlainString (), boldFont8, 1, 1, 3, Element.ALIGN_CENTER));
                }

            }

        }
        if (row < 26) {
            // 不满26；
            for (int i = 0; i < 26 - row; i++) {
                table.addCell (cellBuilder.buildCell ("   ", boldFont8, 1, 1, 3, Element.ALIGN_CENTER));
                table.addCell (cellBuilder.buildCell ("   ", boldFont8, 1, 1, 3, Element.ALIGN_CENTER));
                table.addCell (cellBuilder.buildCell ("   ", boldFont8, 1, 1, 3, Element.ALIGN_CENTER));
                table.addCell (cellBuilder.buildCell ("   ", boldFont8, 1, 1, 3, Element.ALIGN_CENTER));
                table.addCell (cellBuilder.buildCell ("   ", boldFont8, 1, 1, 3, Element.ALIGN_CENTER));
            }
        } else if (row < 35) {
            for (int i = 0; i < 35 - row; i++) {
                if (i == 34 - row) {
                    table.addCell (cellBuilder.buildCell ("   ", boldFont8, 1, 1, 1, Element.ALIGN_CENTER));
                    table.addCell (cellBuilder.buildCell ("   ", boldFont8, 1, 1, 1, Element.ALIGN_CENTER));
                    table.addCell (cellBuilder.buildCell ("   ", boldFont8, 1, 1, 1, Element.ALIGN_CENTER));
                    table.addCell (cellBuilder.buildCell ("   ", boldFont8, 1, 1, 1, Element.ALIGN_CENTER));
                    table.addCell (cellBuilder.buildCell ("   ", boldFont8, 1, 1, 1, Element.ALIGN_CENTER));
                } else {
                    table.addCell (cellBuilder.buildCell ("   ", boldFont8, 1, 1, 3, Element.ALIGN_CENTER));
                    table.addCell (cellBuilder.buildCell ("   ", boldFont8, 1, 1, 3, Element.ALIGN_CENTER));
                    table.addCell (cellBuilder.buildCell ("   ", boldFont8, 1, 1, 3, Element.ALIGN_CENTER));
                    table.addCell (cellBuilder.buildCell ("   ", boldFont8, 1, 1, 3, Element.ALIGN_CENTER));
                    table.addCell (cellBuilder.buildCell ("   ", boldFont8, 1, 1, 3, Element.ALIGN_CENTER));
                }
            }

        }

        // 总计 52
        table.addCell (cellBuilder.buildCell ("总计 ", normalFont8, 1, 2, null, Element.ALIGN_RIGHT));
        table.addCell (cellBuilder.buildCell ("5件", boldFont8, 1, 1, null, Element.ALIGN_RIGHT));
        table.addCell (cellBuilder.buildCell ("xxxKGS", boldFont8, 1, 1, null, Element.ALIGN_RIGHT));
        table.addCell (cellBuilder.buildCell ("XXXGBM", boldFont8, 1, 1, null, Element.ALIGN_RIGHT));
        // 箱型箱量 53
        table.addCell (cellBuilder.buildCell ("箱型箱量: 20GP*1;40GP*2", boldFont8, 1, 2, null, Element.ALIGN_LEFT));
        // 公章
        table.addCell (cellBuilder15.buildCell (getImage ("template/1.png", "png"), 8, 1, 12, Element.ALIGN_RIGHT));
        // 公众号图片
        table.addCell (cellBuilder15.buildCell (getImage ("template/2.png", "png"), 8, 2, 4, Element.ALIGN_RIGHT));
        // 54
        table.addCell (cellBuilder.buildCell ("运输条款: AEWWQE", boldFont8, 1, 1, null, Element.ALIGN_LEFT));
        table.addCell (cellBuilder.buildCell ("M付款方式: AEWWQEWEWQEQ", boldFont8, 1, 1, null, Element.ALIGN_LEFT));
        // 提单类型 55
        table.addCell (cellBuilder.buildCell ("提单类型:xxxxxxxxxxxxx", boldFont8, 1, 2, null, Element.ALIGN_LEFT));

        table.addCell (cellBuilder.buildCell ("     ", boldFont8, 1, 2, 11, Element.ALIGN_LEFT));
        // 客服 57
        table.addCell (cellBuilder.buildCell ("客服: 孙勇", normalFont8, 1, 1, 11, Element.ALIGN_LEFT));
        // 邮箱
        table.addCell (cellBuilder.buildCell ("邮箱: service@echem56.com", blueFont8, 1, 1, 15, Element.ALIGN_LEFT));
        // 座机 58
        table.addCell (cellBuilder.buildCell ("座机: 021-12345678", normalFont8, 1, 1, 11, Element.ALIGN_LEFT));
        // 手机
        table.addCell (cellBuilder.buildCell ("手机: 13912345678", normalFont8, 1, 1, 15, Element.ALIGN_LEFT));
        // 地址 59
        table.addCell (cellBuilder.buildCell ("地址: 上海市浦东新区锦绣东路2777弄锦绣申江39号楼", normalFont8, 1, 2, 11, Element.ALIGN_LEFT));
        // 收尾 60
        table.addCell (cellBuilder.buildCell ("     ", normalFont8, 1, 2, 9, Element.ALIGN_LEFT));

        return table;
    }

    private Image getImage(String uri, String suffix) throws IOException, BadElementException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream ();
        InputStream inputStream = new ClassPathResource (uri).getInputStream ();
        BufferedImage bufferedImage = ImageIO.read (inputStream);
        ImageIO.write (bufferedImage, suffix, baos);
        return Image.getInstance (baos.toByteArray ());
    }
}
