package com.ihcy.doc.pdf.service;

import com.ihcy.doc.pdf.builder.CellBuilder;
import com.ihcy.doc.pdf.builder.FontBuilder;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import io.micrometer.core.instrument.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;


@Service
@Slf4j
public class PdfService3 {


    public void createPdf() {


        /**
         * 指定页面大小为A4，且自定义页边距(marginLeft、marginRight、marginTop、marginBottom)
         * Document document =new Document(PageSize.A4,50,50,30,20);
         */
        // 横版A4
        Document document = new Document(new RectangleReadOnly(842, 595, 90));
        PdfWriter writer = null;
        try {
            // 创建PdfWriter对象
            writer = PdfWriter.getInstance(document, new FileOutputStream("test3.PDF"));
            document.addAuthor("杨波");
            document.addCreationDate();
            document.addKeywords("测试");
            document.addProducer();
            document.addCreator("www.ydc51.com");
            document.addTitle("测试标题");
            document.addSubject("测试PDF创建的主题");
            document.open();
            //添加一个内容段落 ,包含字体
            Paragraph titleParagraph = new Paragraph("付款申请单", FontBuilder.fontBuild(20, Font.BOLD));
            // 居中对齐
            titleParagraph.setAlignment(Element.ALIGN_CENTER);
            titleParagraph.setSpacingAfter(8);
            document.add(titleParagraph);

            // 表格
            document.add(getPdfPTable());

            // 打印人，打印日期
            StringBuilder strb = new StringBuilder("打印人：");
            strb.append("杨波");
            strb.append("           打印日期：");
            strb.append("2020-02-20");
            log.info(strb.toString());
            Paragraph printParagraph = new Paragraph(strb.toString(), FontBuilder.fontBuild(11, Font.NORMAL));
            printParagraph.setSpacingBefore(8);
            printParagraph.setSpacingAfter(5);
            document.add(printParagraph);

            // 审批
            Paragraph auditParagraph = new Paragraph("审批情况", FontBuilder.fontBuild(20, Font.BOLD));
            // 居中对齐
            auditParagraph.setAlignment(Element.ALIGN_CENTER);
            auditParagraph.setSpacingAfter(8);
            document.add(auditParagraph);

            document.add(getAuditTable());

            // 打印人，打印日期
            StringBuilder crateString = new StringBuilder("制表人：");
            crateString.append("杨波");
            crateString.append("           制表日期：");
            crateString.append("2020-02-20");
            Paragraph createParagraph = new Paragraph(crateString.toString(), FontBuilder.fontBuild(11, Font.NORMAL));
            printParagraph.setSpacingBefore(8);
            printParagraph.setSpacingAfter(5);
            document.add(createParagraph);

        } catch (Exception e) {
            log.error("", e);
        } finally {
            // 关闭文档
            document.close();
            assert writer != null;
            writer.close();
        }

    }

    private PdfPTable getPdfPTable() throws DocumentException, IOException {
        // 7列的表.
        PdfPTable table = new PdfPTable(7);
        // 宽度100%填充
        table.setWidthPercentage(100);
        // 前间距
        table.setSpacingBefore(10f);
        // 后间距
        table.setSpacingAfter(5f);

        //设置列宽
        float[] columnWidths = {1f, 1f, 1f, 1f, 1f, 1f, 1f};
        table.setWidths(columnWidths);
        Font BOLDFont = FontBuilder.fontBuild(10, Font.BOLD);
        Font NormalFont = FontBuilder.fontBuild(10);
        CellBuilder cellBuilder_28 = new CellBuilder(21);

        table.addCell(cellBuilder_28.buildCell("申请主体", BOLDFont, Boolean.TRUE));

        table.addCell(cellBuilder_28.buildCell("上海慎则化工科技有限公司",
                FontBuilder.fontBuild(11, Font.BOLD | Font.UNDERLINE),2,Boolean.TRUE));

        table.addCell(cellBuilder_28.buildCell("",
                FontBuilder.fontBuild(11 ),2,Boolean.TRUE));

        table.addCell(cellBuilder_28.buildCell("付款单号", BOLDFont,Boolean.TRUE));

        table.addCell(cellBuilder_28.buildCell("1234567888",
                FontBuilder.fontBuild(11),Boolean.TRUE));

        table.addCell(cellBuilder_28.buildCell("收款单位", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("测试单位", NormalFont,2));
        table.addCell(cellBuilder_28.buildCell("申请日期", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("2020-01-14", NormalFont));
        table.addCell(cellBuilder_28.buildCell("最后付款日", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("2020-02-20", NormalFont));

        table.addCell(cellBuilder_28.buildCell("收款银行", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("测试银行", NormalFont,2));
        table.addCell(cellBuilder_28.buildCell("成本类型", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("正常成本", NormalFont));
        table.addCell(cellBuilder_28.buildCell("币   种", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("人民币", NormalFont));

        table.addCell(cellBuilder_28.buildCell("收款帐号", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("test111111111", NormalFont,2));
        table.addCell(cellBuilder_28.buildCell("原币总额", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("100,000", NormalFont));
        table.addCell(cellBuilder_28.buildCell("本币总额", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("80,000", NormalFont));

        PdfPCell cell1 = new CellBuilder(8).buildCell("",NormalFont);
        cell1.setBackgroundColor(BaseColor.GRAY);
        cell1.setColspan(7);
        table.addCell(cell1);

        table.addCell(cellBuilder_28.buildCell("采购合同", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("申请部门", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("款项类型", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("付款方式", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("原币金额", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("汇率", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("本币金额", BOLDFont));
        for (int i = 0; i < 4; i++) {
            table.addCell(cellBuilder_28.buildCell("采购合同"+i, NormalFont));
            table.addCell(cellBuilder_28.buildCell("申请部门"+i, NormalFont));
            table.addCell(cellBuilder_28.buildCell("款项类型"+i, NormalFont));
            table.addCell(cellBuilder_28.buildCell("付款方式"+i, NormalFont));
            table.addCell(cellBuilder_28.buildCell("原币金额"+i, NormalFont));
            table.addCell(cellBuilder_28.buildCell("汇率"+i, NormalFont));
            table.addCell(cellBuilder_28.buildCell("本币金额"+i, NormalFont));

        }
        return table;
    }

    private PdfPTable getAuditTable() throws DocumentException, IOException {
        // 8列的表.
        PdfPTable table = new PdfPTable(8);
        // 宽度100%填充
        table.setWidthPercentage(100);
        // 前间距
        table.setSpacingBefore(10f);
        // 后间距
        table.setSpacingAfter(5f);
        //设置列宽
        float[] columnWidths = {0.5f, 1f, 0.5f, 0.5f, 0.5f, 1f, 1f,1f};
        table.setWidths(columnWidths);
        Font BOLDFont = FontBuilder.fontBuild(9, Font.BOLD);
        Font NormalFont = FontBuilder.fontBuild(9);
        CellBuilder cellBuilder_28 = new CellBuilder(19);

        table.addCell(cellBuilder_28.buildCell("单据类型", BOLDFont, Boolean.TRUE));

        table.addCell(cellBuilder_28.buildCell("付款",
                FontBuilder.fontBuild(11,  Font.UNDERLINE),Boolean.TRUE));

        table.addCell(cellBuilder_28.buildCell("",
                FontBuilder.fontBuild(11 ),2,Boolean.TRUE));

        table.addCell(cellBuilder_28.buildCell("单据编号", BOLDFont,Boolean.TRUE));

        table.addCell(cellBuilder_28.buildCell("1234567888",
                FontBuilder.fontBuild(11),Boolean.TRUE));

        table.addCell(cellBuilder_28.buildCell("",
                FontBuilder.fontBuild(11 ),2,Boolean.TRUE));


        table.addCell(cellBuilder_28.buildCell("发送人", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("发送日期", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("审批人", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("审批状况", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("审批意见", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("审批日期", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("批语", BOLDFont));
        table.addCell(cellBuilder_28.buildCell("历时", BOLDFont));
        for (int i = 0; i < 4; i++) {
            table.addCell(cellBuilder_28.buildCell("发送人"+i, BOLDFont));
            table.addCell(cellBuilder_28.buildCell("发送日期"+i, BOLDFont));
            table.addCell(cellBuilder_28.buildCell("审批人"+i, BOLDFont));
            table.addCell(cellBuilder_28.buildCell("审批状况"+i, BOLDFont));
            table.addCell(cellBuilder_28.buildCell("审批意见"+i, BOLDFont));
            table.addCell(cellBuilder_28.buildCell("审批日期"+i, BOLDFont));
            table.addCell(cellBuilder_28.buildCell("批语"+i, BOLDFont));
            table.addCell(cellBuilder_28.buildCell("历时"+i, BOLDFont));

        }
        return table;
    }



}

