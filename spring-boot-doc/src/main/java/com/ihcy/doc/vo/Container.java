package com.ihcy.doc.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class Container implements Serializable {
    private static final long serialVersionUID = -9223372036854775808L;

    /**
     * 箱型箱种
     */
    private String ctnType;


    private List<ContainerItem> itemList;

    public Container(String ctnType) {
        this.ctnType = ctnType;
    }
}