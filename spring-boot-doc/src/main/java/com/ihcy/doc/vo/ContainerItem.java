package com.ihcy.doc.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class ContainerItem implements Serializable {
    private static final long serialVersionUID = -9223372036854775808L;

    /**
     * 货物代码
     */
    private String itemCode;

    /**
     * 货物中文名称
     */
    private String itemNameCn;

    /**
     * 货物英文名称
     */
    private String itemNameEn;

    /**
     * 件数
     */
    private Integer quantity;

    /**
     * 毛重(KG)
     */
    private BigDecimal grossWeight;

    /**
     * 净重(KG)
     */
    private BigDecimal weight;

    /**
     * 体积(立方米)
     */
    private BigDecimal volume;

    /**
     * UNNO
     */
    private String unno;

    /**
     * CLASS
     */
    private String classNo;

    /**
     * 是否危险品
     */
    private Boolean dangerFlag;

    /**
     * 危险品等级
     */
    private String dangerousType;

    public ContainerItem(String itemNameEn, Integer quantity, BigDecimal grossWeight, BigDecimal volume) {
        this.itemNameEn = itemNameEn;
        this.quantity = quantity;
        this.grossWeight = grossWeight;
        this.volume = volume;
        this.dangerFlag = Boolean.FALSE;
    }

    public ContainerItem(String itemNameEn, Integer quantity, BigDecimal grossWeight, BigDecimal volume, String unno, String classNo) {
        this.itemNameEn = itemNameEn;
        this.quantity = quantity;
        this.grossWeight = grossWeight;
        this.volume = volume;
        this.unno = unno;
        this.classNo = classNo;
        this.dangerFlag = Boolean.TRUE;
    }
}
