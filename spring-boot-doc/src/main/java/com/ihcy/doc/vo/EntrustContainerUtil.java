package com.ihcy.doc.vo;

import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 26 行 满页 ;  分页至少33 行，26-33 行 补满行，第33行 加下划线
 */
public class EntrustContainerUtil {

    // 不满26行 4+2+4=10
    public static List<Container> getContainerList1() {
        List<Container> containers = new ArrayList<> ();
        Container container20GP = new Container ("20GP");
        Container container40GP = new Container ("40GP");
        containers.add (container20GP);
        containers.add (container40GP);
        List<ContainerItem> items1 = new ArrayList<> ();
        List<ContainerItem> items2 = new ArrayList<> ();
        container20GP.setItemList (items1);
        container40GP.setItemList (items2);
        items1.add (new ContainerItem ("普货-铅笔", 1, BigDecimal.TEN, BigDecimal.TEN));
        items1.add (new ContainerItem ("普货-手机", 2, BigDecimal.ONE, BigDecimal.TEN));
        items1.add (new ContainerItem ("普货-润滑剂", 6, BigDecimal.TEN, BigDecimal.TEN));
        items1.add (new ContainerItem ("危险品-硝酸铵", 9, BigDecimal.TEN, BigDecimal.TEN, "1234", "5.6"));

        items2.add (new ContainerItem ("危险品-纳", 1, BigDecimal.TEN, BigDecimal.TEN, "1234", "5.6"));
        items2.add (new ContainerItem ("普货-润滑剂", 6, BigDecimal.TEN, BigDecimal.TEN));
        items2.add (new ContainerItem ("危险品-硝酸铵", 9, BigDecimal.TEN, BigDecimal.TEN, "1234", "5.6"));


        return containers;
    }

    // 26 行-30行  7+18+3
    public static List<Container> getContainerList2() {
        List<Container> containers = new ArrayList<> ();
        Container container20GP = new Container ("20GP");
        Container container20HP = new Container ("20HP");
        Container container40GP = new Container ("40GP");
        containers.add (container20GP);
        containers.add (container20HP);
        containers.add (container40GP);
        List<ContainerItem> items1 = new ArrayList<> ();
        List<ContainerItem> items2 = new ArrayList<> ();
        List<ContainerItem> items3 = new ArrayList<> ();
        container20GP.setItemList (items1);
        container40GP.setItemList (items2);
        container20HP.setItemList (items3);
        items1.add (new ContainerItem ("普货-铅笔", 1, BigDecimal.TEN, BigDecimal.TEN));
        items1.add (new ContainerItem ("普货-手机", 1, BigDecimal.TEN, BigDecimal.TEN));
        items1.add (new ContainerItem ("普货-鼠标", 1, BigDecimal.TEN, BigDecimal.TEN));
        items1.add (new ContainerItem ("普货-键盘", 2, BigDecimal.ONE, BigDecimal.TEN));
        items1.add (new ContainerItem ("普货-笔记本", 6, BigDecimal.TEN, BigDecimal.TEN));
        items1.add (new ContainerItem ("危险品-硝酸铵1", 9, BigDecimal.TEN, BigDecimal.TEN, "1234", "5.6"));
        items1.add (new ContainerItem ("危险品-硝酸铵2", 9, BigDecimal.TEN, BigDecimal.TEN, "1234", "5.6"));
        items1.add (new ContainerItem ("危险品-硝酸铵3", 9, BigDecimal.TEN, BigDecimal.TEN, "1234", "5.6"));

        items2.add (new ContainerItem ("危险品-纳", 1, BigDecimal.TEN, BigDecimal.TEN, "1234", "5.6"));
        items2.add (new ContainerItem ("普货-润滑剂", 6, BigDecimal.TEN, BigDecimal.TEN));
        items2.add (new ContainerItem ("普货-润剂", 6, BigDecimal.TEN, BigDecimal.TEN));
        items2.add (new ContainerItem ("普货-滑剂", 6, BigDecimal.TEN, BigDecimal.TEN));
        items2.add (new ContainerItem ("危险品-硝酸铵", 9, BigDecimal.TEN, BigDecimal.TEN, "1234", "5.6"));

        items3.add (new ContainerItem ("危险品-纳", 1, BigDecimal.TEN, BigDecimal.TEN, "1234", "5.6"));
        items3.add (new ContainerItem ("危险品-乙醇", 1, BigDecimal.TEN, BigDecimal.TEN, "1234", "5.6"));
        items3.add (new ContainerItem ("危险品-苯", 1, BigDecimal.TEN, BigDecimal.TEN, "1234", "5.6"));
        items3.add (new ContainerItem ("普货-润滑剂", 6, BigDecimal.TEN, BigDecimal.TEN));
        items3.add (new ContainerItem ("危险品-硝酸铵", 9, BigDecimal.TEN, BigDecimal.TEN, "1234", "5.6"));
        return containers;
    }

    // 26-33行
    public static List<Container> getContainerList3() {
        List<Container> containers = getContainerList2 ();
        Container container40HP = new Container ("40HP");
        containers.add (container40HP);
        List<ContainerItem> items1 = new ArrayList<> ();
        container40HP.setItemList (items1);
        items1.add (new ContainerItem ("危险品-a", 9, BigDecimal.TEN, BigDecimal.TEN, "1234", "5.6"));
        items1.add (new ContainerItem ("危险品-b", 9, BigDecimal.TEN, BigDecimal.TEN, "1234", "5.6"));
        items1.add (new ContainerItem ("危险品-c", 9, BigDecimal.TEN, BigDecimal.TEN, "1234", "5.6"));
        items1.add (new ContainerItem ("危险品-d", 9, BigDecimal.TEN, BigDecimal.TEN, "1234", "5.6"));

        return containers;
    }

    public static void getContainerKind(List<Container> containers,
                                        List<Container> dangerContainers,
                                        List<ContainerItem> commonItems) {
        for (Container container : containers) {
            Container target = new Container (container.getCtnType ());
            for (ContainerItem item : container.getItemList ()) {
                if (item.getDangerFlag ()) {
                    if (target.getItemList () == null) {
                        target.setItemList (new ArrayList<> (Arrays.asList (item)));
                    } else {
                        target.getItemList ().add (item);
                    }
                } else {
                    commonItems.add (item);
                }
            }
            if (!CollectionUtils.isEmpty (target.getItemList ())) {
                dangerContainers.add (target);
            }
        }
    }

}
