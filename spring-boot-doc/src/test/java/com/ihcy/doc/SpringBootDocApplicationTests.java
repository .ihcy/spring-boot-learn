package com.ihcy.doc;

import com.ihcy.doc.pdf.service.*;
import com.ihcy.doc.vo.EntrustContainerUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class SpringBootDocApplicationTests {

    @Autowired
    private PdfService pdfService;
    @Autowired
    private PdfService2 pdfService2;
    @Autowired
    private PdfService3 pdfService3;
    @Autowired
    private PdfService4 pdfService4;
    @Autowired
    private PdfService5 pdfService5;
    @Autowired
    private PdfServiceTemp PdfServiceTemp;
    @Autowired
    private EntrustPdfService entrustPdfService;
    @Autowired
    private TestPdfService testPdfService;
    @Autowired
    private InnerPdfService innerPdfService;

    public void pdfService2() {
        pdfService2.createPdf();
    }

    public void pdfService3() {
        pdfService3.createPdf();
    }
    public void pdfService4() {
        pdfService4.createPdf();
    }
    public void test() {
        System.out.println(getMoney(new BigDecimal("31398217985.32311")));
        System.out.println(getMoney(new BigDecimal("31398217985")));
        System.out.println(getMoney(new BigDecimal("31398217985.001")));
        System.out.println(getMoney(new BigDecimal("31398217985.2320000")));
        System.out.println(getMoney(new BigDecimal("1")));
        System.out.println(getMoney(new BigDecimal("0")));
    }

    public void pdfService5() {
        pdfService5.createPdf();
    }

    public void entrustPdfService() {
        entrustPdfService.createPdf (EntrustContainerUtil.getContainerList1 (), "1");
        entrustPdfService.createPdf (EntrustContainerUtil.getContainerList2 (), "2");
        entrustPdfService.createPdf(EntrustContainerUtil.getContainerList3 (),"3");
    }
    @Test
    public void innerPdfService(){
        innerPdfService.createPdf();
    }

//    @Test
    public void TestPdfService() {
        testPdfService.createPdf();
    }

    public void test1() {
        List<String> list = new ArrayList<>();
        for (String s : list) {
            System.out.println(s);
        }
    }
    private String getMoney(BigDecimal bigDecimal) {
        DecimalFormat format = new DecimalFormat("#,###");
        String intString = format.format(bigDecimal);
        String[] s = bigDecimal.stripTrailingZeros().toPlainString().split("\\.");
        if (s.length > 1) {
            intString = intString + "." + s[1];
        }

        return intString;
    }
}
