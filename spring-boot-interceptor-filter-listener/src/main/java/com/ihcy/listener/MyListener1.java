package com.ihcy.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * @ClassName MyListener1
 * @Description
 * @Author ihcy
 * @Date 2019/12/13 16:28
 * @Version 1.0
 **/
@WebListener
public class MyListener1 implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("******MyListener1 ...contextInitialized ");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("******MyListener1 ...contextDestroyed ");

    }
}
