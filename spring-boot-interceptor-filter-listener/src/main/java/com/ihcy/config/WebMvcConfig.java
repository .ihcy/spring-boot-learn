package com.ihcy.config;

import com.ihcy.interceptor.MyInterceptor1;
import com.ihcy.interceptor.MyInterceptor2;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @ClassName WebMvcConfig 配置拦截器
 * @Description
 * @Author ihcy
 * @Date 2019/12/11 10:31
 * @Version 1.0
 **/
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        /**
         * 顺序，先进后出，和aop的环形切面一致
         * MyInterceptor1 preHandle
         * MyInterceptor2 preHandle
         * MyInterceptor2 postHandle
         * MyInterceptor1 postHandle
         * MyInterceptor2 afterCompletion
         * MyInterceptor1 afterCompletion
         */
        registry.addInterceptor(new MyInterceptor1()).addPathPatterns("/interceptor/*");
        registry.addInterceptor(new MyInterceptor2()).addPathPatterns("/interceptor/*");
    }
}
