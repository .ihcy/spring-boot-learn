package com.ihcy.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * @ClassName MyFilter1
 * @Description
 * @Author ihcy
 * @Date 2019/12/13 15:51
 * @Version 1.0
 **/
@WebFilter(urlPatterns = {"/filter/*"},asyncSupported = true)
public class MyFilter1 implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("********************"+
                filterConfig.getInitParameter("initParam")+
                "***************************");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        System.out.println("**************doFilter1*************");
        chain.doFilter(request,response);
    }

    @Override
    public void destroy() {
        System.out.println("**************destroy**************");
    }
}
