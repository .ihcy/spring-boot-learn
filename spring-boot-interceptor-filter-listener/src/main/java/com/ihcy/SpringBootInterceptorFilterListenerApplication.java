package com.ihcy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan({"com.ihcy.filter","com.ihcy.listener"})
public class SpringBootInterceptorFilterListenerApplication {

	/**
	 * @ServletComponentScan("com.ihcy.filter")
	 * filter 需要扫描注册
	 * listener 扫描注册
	 */

	public static void main(String[] args) {
		SpringApplication.run(SpringBootInterceptorFilterListenerApplication.class, args);
	}

}
