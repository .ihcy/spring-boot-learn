package com.ihcy.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.io.OutputStream;
import java.util.concurrent.Future;

/**
 * @ClassName FilterController
 * @Description
 * @Author ihcy
 * @Date 2019/12/11 10:32
 * @Version 1.0
 **/
@RestController
@RequestMapping("/filter")
public class FilterController {

    @GetMapping(value = "/hello1")
    public ResponseEntity<String> hello() throws InterruptedException {
        return ResponseEntity.ok("HelloWorld");
    }

    @GetMapping(value = "/hello2")
    public StreamingResponseBody hello2() throws InterruptedException {
        Thread.sleep(500);
        // 触发afterConcurrentHandlingStarted
        return (OutputStream outputStream) -> {
            outputStream.write("success".getBytes());
            outputStream.flush();
            outputStream.close();
        };
    }

    @GetMapping(value = "/hello3")
    public Future<String> hello3() throws InterruptedException {
        Thread.sleep(500);
        // 触发 afterConcurrentHandlingStarted
        return new AsyncResult<>("Hello");
    }
}
