> 个人关于spring boot 的学习
# Spring Boot Demo
## spring boot config
### 1. 环境配置
实现环境配置方案有2中
* 如现在所示，在application_{profile}.yml 中定义每个环境的配置，然后在application.yml中指定环境
* 使用--- 进行分离
```yaml
spring:
  profiles:
    active: pro
server:
  port: 8081
---
spring:
  profiles:
    active: test
server:
  port: 8081
---
spring:
  profiles:
    active: dev
server:
  port: 8081
```
* 在类上使用@Profile 注解，只会加载指定环境下的Service类

### 2. 读取变量值
* 可以在yml配置文件里开始读取
* 可以单独写一个properties文件，读取该配置文件
* 使用@Value 注解读取

### 3.自定义异常页面

#### 3.1自定义异常页面
  对于 404、 405、 500 等异常状态，服务器会给出默认的异常页面，而这些异常页面一般
  都是英文的，且非常不友好。我们可以通过简单的方式使用自定义异常页面，并将默认状态
  码页面进行替换。
  直接在前面程序上修改即可，无需创建新的工程。
#####  3.1.1 定义目录
  在 src/main/resources 目录下再定义新的目录 public/error，必须是这个目录名称。 是/ 不是.
#####  3.1.2 定义异常页面
  在 error 目录中定义异常页面。这些异常页面的名称必须为相应的状态码，扩展名为 html。

### 4. 监控中心 actuator 配置
```yaml
# actuator 的配置
management:
  server:
    port: 9999   # 监控的端口号
    servlet:
      context-path: /ihcy # 监控的上下文路径
  endpoints:
    web:
      #      base-path: /base #监控终端的基本路径，默认actuator，一般不修改
      exposure:
        include: "*" # yml 不允许直接使用*号，需要加双引号。 可以使用其他终端，默认只开启health、info
        exclude: ['env','beans'] # 也可写为 env,beans  不让查看的终端
```
### 5. 使用jsp 文件
* 添加解析配置
```xml
  <dependency>
      <groupId>org.apache.tomcat.embed</groupId>
      <artifactId>tomcat-embed-jasper</artifactId>
  </dependency>
```
* 配置资源文件目录
```xml
        <resources>
            <!--注册webapp 目录为资源目录-->
            <resource>
                <directory>src/main/webapp</directory>
                <targetPath>META-INF/resources</targetPath>
                <includes>
                    <include>**/*.*</include>
                </includes>
            </resource>
        </resources>
```
* 逻辑视图前后缀配置
```yaml
spring:
  mvc:
    view:
      prefix: / # 前缀
      suffix: .jsp # 后缀
```
