package com.ihcy.service;

import com.ihcy.enums.ServiceType;

public interface ICommonServiceFactory {
    ICommonService getCommonService(ServiceType serviceType);
}
