package com.ihcy.service;

/**
 * @InterfaceName IDemoService
 * @Description
 * @Author ihcy
 * @Date 2019/8/25 17:02
 * @Version 1.0
 **/
public interface IDemoService {

    void doSomeThing();

    String getName();

    String getTestValue();

    void printPerson();

    void getListValue();

    void testSomeService(String string);
}

