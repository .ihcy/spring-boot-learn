package com.ihcy.service;

/**
 * @InterfaceName IProfileService
 * @Description 子类配置不同的profile。可以在不同的环境调用不同的子类
 * @Author ihcy
 * @Date 2019/8/25 17:02
 * @Version 1.0
 **/
public interface IProfileService {

    String doSomeThing();
}

