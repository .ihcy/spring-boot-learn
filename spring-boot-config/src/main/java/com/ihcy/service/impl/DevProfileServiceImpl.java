package com.ihcy.service.impl;

import com.ihcy.service.IProfileService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

/**
 * @ClassName DevProfileService
 * @Description
 * @Author ihcy
 * @Date 2019/8/25 17:04
 * @Version 1.0
 **/
@Service
@Profile("dev")
public class DevProfileServiceImpl implements IProfileService {

    @Override
    public String doSomeThing() {
        return "this is dev profile";
    }
}

