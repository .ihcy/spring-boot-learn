package com.ihcy.service.impl;

import com.ihcy.enums.ServiceType;
import com.ihcy.service.ICommonService;
import com.ihcy.service.ICommonServiceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
/**
 * 多种注入方式
 * 1.
 *     @Resource(type = NormalServiceImpl.class)
 *     private ICommonService normalService;
 *
 *     @Resource(type = VipServiceImpl.class)
 *     private ICommonService vipService;
 *
 *     @Resource(type = SVipServiceImpl.class)
 *     private ICommonService sVipService;
 *
 * 2.
 *      @Resource(name = "normalService")
 *      private ICommonService normalService;
 *
 *      @Service("normalService")
 *      public class NormalServiceImpl implements ICommonService
 *
 *  3. 自动识别
 *     @Resource
 *     private ICommonService normalService;
 *
 *     @Resource
 *     private ICommonService vipService;
 *
 *     @Resource
 *     private ICommonService sVipService;
 *
 *  4.  @Autowired
 *     @Qualifier("normalService")
 *     private ICommonService normalService;
 *
 *     @Service("normalService")
 *     public class NormalServiceImpl implements ICommonService
 *
 *  5. 构造器注入
 *    private ICommonService normalService;
 *
 *     private ICommonService vipService;
 *
 *     private ICommonService sVipService;
 *
 *     public CommonFactory(ICommonService normalService, ICommonService vipService, ICommonService sVipService) {
 *         this.normalService = normalService;
 *         this.vipService = vipService;
 *         this.sVipService = sVipService;
 *     }
 *
 *  6. 构造器注入 2
 *      private ICommonService normalService;
 *
 *     private ICommonService vipService;
 *
 *     private ICommonService sVipService;
 *
 *     public CommonFactory(NormalServiceImpl normalService, VipServiceImpl vipService, SVipServiceImpl sVipService) {
 *         this.normalService = normalService;
 *         this.vipService = vipService;
 *         this.sVipService = sVipService;
 *     }
 */

@Component
public class CommonFactory implements ICommonServiceFactory {

    private ICommonService normalService;

    private ICommonService vipService;

    private ICommonService sVipService;

    public CommonFactory(NormalServiceImpl normalService, VipServiceImpl vipService, SVipServiceImpl sVipService) {
        this.normalService = normalService;
        this.vipService = vipService;
        this.sVipService = sVipService;
    }

    @Override
    public ICommonService getCommonService(ServiceType serviceType) {
        switch (serviceType) {
            case NORMAl:
                return normalService;
            case VIP:
                return vipService;
            case SVIP:
                return sVipService;
            default:
                return null;
        }
    }
}
