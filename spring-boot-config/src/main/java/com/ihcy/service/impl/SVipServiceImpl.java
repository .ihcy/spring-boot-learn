package com.ihcy.service.impl;

import com.ihcy.service.ICommonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class SVipServiceImpl implements ICommonService {
    @Override
    public void service() {
        log.info("this is SVipService");
    }
}
