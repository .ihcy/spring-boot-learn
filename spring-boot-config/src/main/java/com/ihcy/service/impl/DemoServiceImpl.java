package com.ihcy.service.impl;

import com.ihcy.dto.Person;
import com.ihcy.service.IDemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName DemoServiceImpl
 * @Description
 * @Author ihcy
 * @Date 2019/8/25 17:05
 * @Version 1.0
 **/
@Service
@PropertySource(value = "classpath:config.properties",encoding = "utf-8")
public class DemoServiceImpl implements IDemoService {

    /**
     * 读取 yml 里的内容
     */
    @Value("${info.author.name}")
    private String authorName;

    /**
     * 读取config.properties 中的内容
     */
    @Value("${value}")
    private String testValue;

    @Value("#{'${list.value}'.split(',')}")
    private List<Integer> num;

    @Autowired
    private Person person;

//    @Autowired
//    private StringService stringService;

    @Override
    public void doSomeThing() {
        System.out.println("Hello DemoService");
    }
    @Override
    public String getName(){
        return "info.author.name: "+authorName;
    }

    @Override
    public String getTestValue() {
        return "value: "+testValue;
    }

    @Override
    public void printPerson() {
        System.out.println(person);
    }

    @Override
    public void getListValue() {
        System.out.println(num);
    }

    @Override
    public void testSomeService(String string) {
//        System.out.println(stringService.spliceString(string));
    }
}
