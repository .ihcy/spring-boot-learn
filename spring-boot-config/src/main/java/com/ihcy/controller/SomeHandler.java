package com.ihcy.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @ClassName SomeHandler
 * @Description
 * @Author ihcy
 * @Date 2019/12/2 10:04
 * @Version 1.0
 **/
@Controller
@RequestMapping("/test")
public class SomeHandler {

    @PostMapping("/register")
    public String registerHandler(String name, String age, Model model){
        System.out.println(name+","+age);
        model.addAttribute("name",name);
        model.addAttribute("age",age);
        return "jsp/welcome";
    }
}
