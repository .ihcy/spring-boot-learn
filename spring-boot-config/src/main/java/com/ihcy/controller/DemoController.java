package com.ihcy.controller;

import com.ihcy.service.IDemoService;
import com.ihcy.service.IProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName DemoController
 * @Description
 * @Author ihcy
 * @Date 2019/8/25 17:01
 * @Version 1.0
 **/
@RestController
public class DemoController {

    @Autowired
    private IProfileService profileService;
    @Autowired
    private IDemoService demoService;

    @RequestMapping("/demo")
    public String demo(){
        return "Hello Spring Boot World";
    }

    @RequestMapping("/getName")
    public String getName(){
        return demoService.getName();
    }

    @RequestMapping("/profile")
    public String profile(){
        return profileService.doSomeThing();
    }
}
