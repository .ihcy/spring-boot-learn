package com.ihcy.dto;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @ClassName Person
 * @Description 测试对象读取
 * @Author ihcy
 * @Date 2019/8/25 17:01
 * @Version 1.0
 **/
@Component
@PropertySource(value = "classpath:config.properties",encoding = "utf-8")
@ConfigurationProperties("person")
@Data
public class Person {

    private String name;

    private int age;

    private double score;

    private String[] tels;
}
