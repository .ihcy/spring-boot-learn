package com.ihcy;

import com.ihcy.enums.ServiceType;
import com.ihcy.service.ICommonServiceFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ICommonServiceTest {

    @Autowired
    private ICommonServiceFactory commonServiceFactory;

    @Test
    public void test(){
        commonServiceFactory.getCommonService(ServiceType.NORMAl).service();
        commonServiceFactory.getCommonService(ServiceType.VIP).service();
        commonServiceFactory.getCommonService(ServiceType.SVIP).service();
    }
}
