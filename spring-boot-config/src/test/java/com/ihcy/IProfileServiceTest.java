package com.ihcy;

import com.ihcy.service.IProfileService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @ClassName IProfileServiceTest
 * @Description
 * @Author ihcy
 * @Date 2019/8/25 17:08
 * @Version 1.0
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class IProfileServiceTest {

    @Autowired
    @Qualifier("proProfileServiceImpl")
    private IProfileService profileService;

    @Test
    public void doTest(){
        profileService.doSomeThing();
    }
}

