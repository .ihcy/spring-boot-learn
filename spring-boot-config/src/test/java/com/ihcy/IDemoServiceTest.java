package com.ihcy;

import com.ihcy.service.IDemoService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @ClassName IDemoServiceTest
 * @Description
 * @Author ihcy
 * @Date 2019/8/25 17:07
 * @Version 1.0
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class IDemoServiceTest {

    @Autowired
    private IDemoService demoService;

    @Test()
    public void test(){
        demoService.doSomeThing();
    }

    @Test()
    public void getName(){
        System.out.println(demoService.getName());
    }
    @Test()
    public void getTestValue(){
        System.out.println(demoService.getTestValue());
    }

    @Test
    public void printPerson(){
        demoService.printPerson();
    }

    @Test
    public void getListValue(){
        demoService.getListValue();
    }

    @Test
    public void testSomeService(){
        demoService.testSomeService("hello ketty");
    }
}