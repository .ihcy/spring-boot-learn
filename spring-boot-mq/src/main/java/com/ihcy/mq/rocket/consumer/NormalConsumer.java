package com.ihcy.mq.rocket.consumer;

import com.ihcy.mq.rocket.config.AbstractMQConsumer;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.common.Pair;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.remoting.common.RemotingHelper;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;

@Service
@Slf4j
public class NormalConsumer extends AbstractMQConsumer {
    @Override
    public String getMQName() {
        return "NormalConsumer";
    }

    @Override
    public Pair<String, String> getTopic() {
        return new Pair<>("NormalTopic", "*");
    }

    @Override
    public void execute(MessageExt messageExt) throws UnsupportedEncodingException {
        String str = new String(messageExt.getBody(), RemotingHelper.DEFAULT_CHARSET);
        log.info("NormalConsumer {}", str);

    }
}
