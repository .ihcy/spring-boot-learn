package com.ihcy.mq.rocket.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
@ConfigurationProperties("rocketmq.consumer")
public class MQConsumerConfiguration {
    private String nameServerAddr;
    private String groupName;
    private int consumeThreadMin;
    private int consumeThreadMax;
    private int consumeMessageBatchMaxSize;
}
