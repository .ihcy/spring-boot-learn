package com.ihcy.mq.rocket.controller;

import io.swagger.annotations.Api;
import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.exception.RemotingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "测试RocketMQ")
@RestController
@RequestMapping("mq/rocket")
public class ProducerController {

    @Autowired
    private DefaultMQProducer defaultMQProducer;
    @Value("${rocketmq.topic.normal}")
    private String normalTopic;
    @Value("${rocketmq.topic.exception}")
    private String exceptionTopic;

    @PostMapping("sendNormalTopic")
    public void sendNormalTopic() throws InterruptedException, RemotingException, MQClientException, MQBrokerException {
        Message message = new Message(normalTopic, "*", "test normal".getBytes());
        defaultMQProducer.send(message);
    }

    @PostMapping("sendExceptionTopic")
    public void sendExceptionTopic() throws InterruptedException, RemotingException, MQClientException, MQBrokerException {
        Message message = new Message(normalTopic, "*", "test exception".getBytes());
        defaultMQProducer.send(message);
    }


}
