package com.ihcy.mq.rocket.consumer;

import com.ihcy.mq.rocket.config.AbstractMQConsumer;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.common.Pair;
import org.apache.rocketmq.common.message.MessageExt;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;

@Service
@Slf4j
public class ExceptionConsumer extends AbstractMQConsumer {
    @Override
    public String getMQName() {
        return "ExceptionConsumer";
    }

    @Override
    public Pair<String, String> getTopic() {
        return new Pair<>("ExceptionTopic", "*");
    }

    @Override
    public void execute(MessageExt messageExt) throws UnsupportedEncodingException {
        log.info("ExceptionConsumer  start");
        int i = 1/0;
    }
}
