package com.ihcy.mybatis.service.impl;

import com.github.pagehelper.PageInfo;
import com.ihcy.mybatis.domain.Login;
import com.ihcy.mybatis.domain.User;
import com.ihcy.mybatis.service.ILoginService;
import com.ihcy.mybatis.service.IUserService;
import lombok.SneakyThrows;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.CountDownLatch;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UserServiceImplTest {

    @Autowired
    private IUserService userService;
    @Autowired
    private ILoginService loginService;

    @SneakyThrows
//    @Test
    public void insert() {

        CountDownLatch countDownLatch = new CountDownLatch(2000);
        // 200个线程，每个线程插入10000条数据
        for (int i = 0; i < 2000; i++) {
            userService.task(countDownLatch);
        }
        countDownLatch.await();
        System.out.printf("插入结束");
//        userService.addBatchUser(userService.getBatchUser(3000000));

    }

    @Test
    public void testPage() {
        System.out.println(loginService.getAll().size());

        PageInfo<Login> pageInfo = loginService.page();

        System.out.println(pageInfo.getList());
        System.out.println(pageInfo.getPageNum());
        System.out.println(pageInfo.getPageSize());
        System.out.println(pageInfo.getTotal());
        System.out.println(pageInfo.getPages());

    }

}