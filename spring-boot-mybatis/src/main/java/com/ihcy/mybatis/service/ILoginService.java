package com.ihcy.mybatis.service;

import com.github.pagehelper.PageInfo;
import com.ihcy.mybatis.domain.Login;

import java.util.List;

public interface ILoginService {
    List<Login> getAll();

    PageInfo<Login> page();
}
