package com.ihcy.mybatis.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ihcy.mybatis.domain.Login;
import com.ihcy.mybatis.domain.LoginExample;
import com.ihcy.mybatis.domain.User;
import com.ihcy.mybatis.domain.UserExample;
import com.ihcy.mybatis.mapper.LoginMapper;
import com.ihcy.mybatis.service.ILoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LoginServiceImpl implements ILoginService {

    @Autowired
    private LoginMapper loginMapper;
    @Override
    public List<Login> getAll() {
        LoginExample example = new LoginExample();
        LoginExample.Criteria criteria  = example.createCriteria();
        return loginMapper.selectByExample(new LoginExample());
    }

    @Override
    public PageInfo<Login> page() {
        PageHelper.startPage(2, 6);
        LoginExample example = new LoginExample();
        LoginExample.Criteria criteria  = example.createCriteria();
        List<Login> list = loginMapper.selectByExample(example);
        PageInfo<Login> userPageInfo = new PageInfo<>(list);
        return userPageInfo;
    }



}
