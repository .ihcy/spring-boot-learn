package com.ihcy.mybatis.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ihcy.mybatis.domain.User;
import com.ihcy.mybatis.domain.UserExample;
import com.ihcy.mybatis.mapper.UserMapper;
import com.ihcy.mybatis.service.IUserService;
import com.ihcy.mybatis.utils.UserInfoGeneratorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;

@Service
public class UserServiceImpl implements IUserService {
    private static String[] telFirst = "134,135,136,137,138,139,150,151,152,157,158,159,130,131,132,155,156,133,153".split(",");

    @Autowired
    private UserMapper userMapper;

    // 插入1000个数据
    @Override
    @Async("asyncServiceExecutor")
    public void task(CountDownLatch latch) {
        addBatchUser(getBatchUser(1000));
        latch.countDown();
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void addBatchUser(List<User> list) {
        if (list != null && list.size() > 0) {
            for (User user : list) {
                userMapper.insertSelective(user);
            }
        }
    }

    @Override
    public List<User> getBatchUser(int num) {
        if (num <= 0) {
            return null;
        }
        List<User> users = new LinkedList<>();
        for (int i = 0; i < num; i++) {
            //  获取随机名称
            User user = new User();
            user.setName(UserInfoGeneratorUtil.getRandomName());
            user.setNickName(user.getName().substring(1));
            user.setPassword(UUID.randomUUID().toString());
            user.setMobile(UserInfoGeneratorUtil.getRandomMobile());
            user.setDeptId(new Double(Math.random()).longValue());
            user.setCreateBy(user.getName());
            user.setCreateTime(new Date());
            users.add(user);
        }
        return users;
    }

}
