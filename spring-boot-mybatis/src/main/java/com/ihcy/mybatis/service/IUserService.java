package com.ihcy.mybatis.service;

import com.ihcy.mybatis.domain.User;

import java.util.List;
import java.util.concurrent.CountDownLatch;

public interface IUserService {


    void addBatchUser(List<User> list);

    List<User> getBatchUser(int num);

    void task(CountDownLatch latch);
}
