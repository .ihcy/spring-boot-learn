package com.ihcy.log.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName LogController
 * @Description
 * @Author ihcy
 * @Date 2019/12/17 10:20
 * @Version 1.0
 **/
@RestController
public class LogController {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(LogController.class);


    @GetMapping("/hello")
    public String hello(){
        LOGGER.info("Log-SERVICE:info");
        LOGGER.error("Log-SERVICE:error");
        return "hello spring boot";
    }
}
