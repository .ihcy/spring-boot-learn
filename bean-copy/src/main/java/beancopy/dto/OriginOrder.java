package beancopy.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * @ClassName OriginOrder
 * @Description
 * @Author ihcy
 * @Date 2020/1/12 17:15
 * @Version 1.0
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OriginOrder {

    private Integer id;

    private String orderNo;

    private String address;

    private Date createdTime;

    private String creator;

    private Boolean flag;

    private String stringToInteger;

    private Integer integerToString;

    private String stringToDate;

    private Date dateToString;

    private String code;

    private String name1;

    private OriginDemo demo;

    private List<OriginOrderItem> items;

    public OriginOrder(Integer id, String orderNo, String address, Date createdTime, String creator, Boolean flag) {
        this.id = id;
        this.orderNo = orderNo;
        this.address = address;
        this.createdTime = createdTime;
        this.creator = creator;
        this.flag = flag;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class OriginOrderItem{

        private Integer id;

        private String orderItemNo;

        private String itemName;

        private Integer num;

        private Date createdTime;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class OriginDemo{
        private String name;

    }
}
