package beancopy.dto;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EntityMethod {

    public  List<OriginOrder> getOriginOrders() {
        List<OriginOrder> originOrders = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            OriginOrder originOrder = new OriginOrder(
                    i, "order_" + i, "address_" + i, new Date(), "customer" + i, i / 2 == 0
            );
            List<OriginOrder.OriginOrderItem> items = new ArrayList<>();
            for (int j = 0; j < 5; j++) {
                OriginOrder.OriginOrderItem item = new OriginOrder.OriginOrderItem(
                        j, originOrder.getOrderNo() + "j", "item" + i + j, j, originOrder.getCreatedTime()
                );
                items.add(item);
            }
            originOrder.setItems(items);
            originOrder.setDemo(new OriginOrder.OriginDemo("testDemo"));
            originOrders.add(originOrder);
        }
        return originOrders;
    }

    private  Boolean test(TargetOrder targetOrder,OriginOrder originOrder){
        String originJson = JSON.toJSONString(originOrder, SerializerFeature.SortField);
        String targetJson = JSON.toJSONString(targetOrder, SerializerFeature.SortField);
        System.out.println(originJson);
        System.out.println(targetJson);
        return originJson.equals(targetJson);
    }
}
