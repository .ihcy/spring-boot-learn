package beancopy.util;

import beancopy.dto.OriginOrder;
import beancopy.dto.TargetOrder;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.springframework.beans.BeanUtils;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @ClassName BeanUtils Demo
 * @Description
 * @Author ihcy
 * @Date 2020/1/12 17:43
 * @Version 1.0
 **/
public class Demo {

    public static void main(String[] args) {
        mapFiled();
    }

    /**
     * 1. 属性名称映射  支持忽略映射,支持同类型，同名称；List不同类型copy,List不能操作,List同泛型可以操作
     * 2. 类型的转换 不支持
     * 3. 忽略拷贝 不支持
     * 4. 递归拷贝 不支持
     * 5. 数组和集合拷贝 不支持
     */
    public static void mapFiled() {
        List<OriginOrder> originOrders = getOriginOrders();
        OriginOrder originOrder = originOrders.get(0);
        TargetOrder targetOrder = new TargetOrder();
        BeanUtils.copyProperties(originOrder, targetOrder);
        System.out.println(test(targetOrder,originOrder));
        originOrder.setStringToDate("2018-05-06");
        originOrder.setStringToInteger("1");
        originOrder.setDateToString(new Date());
        originOrder.setIntegerToString(1);
        targetOrder = new TargetOrder();
        BeanUtils.copyProperties(originOrder, targetOrder);

        OriginOrder originOrder1 = new OriginOrder();
        BeanUtils.copyProperties(originOrder, originOrder1);
        for (OriginOrder.OriginOrderItem item : originOrder1.getItems()) {
            item.setItemName("test");
        }
        System.out.println(originOrder1);

        List<TargetOrder> targetOrders = new ArrayList<>();
        BeanUtils.copyProperties(originOrders,targetOrders);
        System.out.println(targetOrders);
    }


    public static List<OriginOrder> getOriginOrders() {
        List<OriginOrder> originOrders = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            OriginOrder originOrder = new OriginOrder(
                    i, "order_" + i, "address_" + i, new Date(), "customer" + i, i / 2 == 0
            );
            List<OriginOrder.OriginOrderItem> items = new ArrayList<>();
            for (int j = 0; j < 5; j++) {
                OriginOrder.OriginOrderItem item = new OriginOrder.OriginOrderItem(
                        j, originOrder.getOrderNo() + "j", "item" + i + j, j, originOrder.getCreatedTime()
                );
                items.add(item);
            }
            originOrder.setItems(items);
            originOrder.setDemo(new OriginOrder.OriginDemo("testDemo"));
            originOrders.add(originOrder);
        }
        return originOrders;
    }

    private static Boolean test(TargetOrder targetOrder,OriginOrder originOrder){
        String originJson = JSON.toJSONString(originOrder, SerializerFeature.SortField);
        String targetJson = JSON.toJSONString(targetOrder, SerializerFeature.SortField);
        System.out.println(originJson);
        System.out.println(targetJson);
        return originJson.equals(targetJson);
    }
}
