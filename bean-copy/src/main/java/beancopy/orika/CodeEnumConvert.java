package beancopy.orika;

import beancopy.dto.CodeEnum;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

public class CodeEnumConvert extends BidirectionalConverter<CodeEnum,Integer> {

    @Override
    public Integer convertTo(CodeEnum source, Type<Integer> destinationType, MappingContext mappingContext) {
        return source.getNumber();
    }

    @Override
    public CodeEnum convertFrom(Integer source, Type<CodeEnum> destinationType, MappingContext mappingContext) {
        for (CodeEnum value : CodeEnum.values()) {
            if(value.getNumber().equals(source)){
                return value;
            }
        }
        return null;
    }
}
