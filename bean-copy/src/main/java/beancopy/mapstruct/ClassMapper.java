package beancopy.mapstruct;

import beancopy.dto.OriginClass;
import beancopy.dto.TargetClass;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

/**
 * @InterfaceName ClassMapper
 * @Description
 * @Author ihcy
 * @Date 2020/1/14 21:02
 * @Version 1.0
 **/
@Mapper(uses = {CodeEnumMapper.class})
public interface ClassMapper {


    @Mappings({
            @Mapping(source = "stdname",target = "name"),
            @Mapping(source = "stdid",target = "id"),
            @Mapping(source = "time",target = "time",dateFormat = "yyyy-MM-dd")
    })
    TargetClass.Student clone(OriginClass.Std std);

    @Mappings({
            @Mapping(source = "classid",target = "id"),
            @Mapping(source = "classname",target = "className"),
            @Mapping(source = "num",target = "number"),
            @Mapping(source = "stds",target = "students"),
            @Mapping(source = "code",target = "codeEnum"),
            @Mapping(source = "tea",target = "teacher"),
//          @Mapping(target = "teacher",ignore = true),
            @Mapping(source = "date",target = "time",dateFormat = "yyyy-MM-dd")
    })
    TargetClass clone(OriginClass originClass);

    List<TargetClass> clone(List<OriginClass> originClasses);


}
