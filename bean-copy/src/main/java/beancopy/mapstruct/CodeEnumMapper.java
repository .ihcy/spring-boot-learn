package beancopy.mapstruct;

import beancopy.dto.CodeEnum;
import org.springframework.stereotype.Service;

/**
 * @ClassName CodeEnumMapper
 * @Description
 * @Author ihcy
 * @Date 2020/1/14 21:14
 * @Version 1.0
 **/
@Service
public class CodeEnumMapper {

    public Integer enumToInteger(CodeEnum codeEnum) {
        return codeEnum.getNumber();
    }

    public CodeEnum integerToEnum (Integer code) {
        CodeEnum codeEnum = null;
        for (CodeEnum value : CodeEnum.values()) {
            if (value.getNumber().equals(code)) {
                codeEnum = value;
            }
        }
        return codeEnum;
    }
}
