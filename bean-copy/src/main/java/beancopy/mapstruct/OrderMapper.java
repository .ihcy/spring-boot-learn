package beancopy.mapstruct;

import beancopy.dto.OriginOrder;
import beancopy.dto.TargetOrder;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses ={DateMapper.class} )
public interface OrderMapper {

    OrderMapper INSTANCE = Mappers.getMapper(OrderMapper.class);


//    TargetOrder.TargetOrderItem clone(OriginOrder.OriginOrderItem item);
//
//    List<TargetOrder.TargetOrderItem> clone(List<OriginOrder.OriginOrderItem> itemList);

    /**
     * 字段相同
     * @return
     */
    @Mapping(source = "name1",target = "name2")
    TargetOrder clone(OriginOrder order);


    List<TargetOrder> clone(List<OriginOrder> orders);



}
