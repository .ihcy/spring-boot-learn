package com.ihcy.integrate.generator;

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

/**
 * @ClassName CodeGenerator
 * @Description mybatis plus 代码生成器
 * @Author ihcy
 * @Date 2019/12/5 11:12
 * @Version 1.0
 **/
public class CodeGenerator {

    public static void main(String[] args) {
        AutoGenerator autoGenerator = new AutoGenerator();
        autoGenerator.setGlobalConfig(getGlobalConfig());
        autoGenerator.setDataSource(getDataSourceConfig());
        autoGenerator.setPackageInfo(getPackageConfig());
        autoGenerator.setStrategy(getStrategyConfig());
        autoGenerator.execute();
    }

    /**
     * 配置 GlobalConfig
     */
    private static GlobalConfig getGlobalConfig(){
        GlobalConfig globalConfig = new GlobalConfig();
        // 生成文件的输出目录【默认 D 盘根目录】
        // System.getProperty("user.dir") 当前工程路径
        globalConfig.setOutputDir(System.getProperty("user.dir")+"/src/main/java");
        // 开发人员
        globalConfig.setAuthor("ihcy");
        // 是否打开输出目录
        globalConfig.setOpen(false);

        return globalConfig;
    }

    /**
     * 获取数据库配置
     */
    private static DataSourceConfig getDataSourceConfig(){
        DataSourceConfig config = new DataSourceConfig();
        config.setUrl("jdbc:mysql://xxx.xxx.x.x:3306/spring_boot?useUnicode=true&characterEncoding=utf-8&serverTimezone=Asia/Shanghai");
        config.setDriverName("com.mysql.cj.jdbc.Driver");
        config.setUsername("root");
        config.setPassword("root");
        return config;
    }

    /**
     * 包名配置
     */
    private static PackageConfig getPackageConfig(){
        PackageConfig packageConfig = new PackageConfig();
        // 父包模块名
        packageConfig.setParent("com.ihcy.integrate");
        return packageConfig;
    }

    /**
     * 策略配置
     */
    private static StrategyConfig getStrategyConfig(){
        StrategyConfig strategy = new StrategyConfig();
        // 类驼峰命名
        strategy.setNaming(NamingStrategy.underline_to_camel);
        // 属性字段驼峰命名
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        // Lombok 注解
        strategy.setEntityLombokModel(true);
        // 跳过视图 RestController 去除
        strategy.isSkipView();
        return strategy;
    }




}
