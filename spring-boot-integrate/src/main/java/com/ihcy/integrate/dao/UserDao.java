package com.ihcy.integrate.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ihcy.integrate.entity.UserEntity;

/**
 * @ClassName UserDao
 * @Description
 * @Author ihcy
 * @Date 2019/12/2 18:45
 * @Version 1.0
 **/
public interface UserDao extends BaseMapper<UserEntity> {
}
