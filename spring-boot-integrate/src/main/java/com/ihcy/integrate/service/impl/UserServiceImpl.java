package com.ihcy.integrate.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ihcy.integrate.dao.UserDao;
import com.ihcy.integrate.entity.UserEntity;
import com.ihcy.integrate.service.IUserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @ClassName UserServiceImpl
 * @Description
 * @Author ihcy
 * @Date 2019/12/2 18:47
 * @Version 1.0
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class UserServiceImpl extends ServiceImpl<UserDao,UserEntity> implements IUserService {
}
