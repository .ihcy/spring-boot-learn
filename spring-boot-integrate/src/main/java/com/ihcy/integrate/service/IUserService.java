package com.ihcy.integrate.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ihcy.integrate.entity.UserEntity;

/**
 * @ClassName IUserService
 * @Description
 * @Author ihcy
 * @Date 2019/12/2 18:46
 * @Version 1.0
 **/
public interface IUserService extends IService<UserEntity> {
}
