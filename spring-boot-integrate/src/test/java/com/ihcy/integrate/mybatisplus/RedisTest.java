package com.ihcy.integrate.mybatisplus;

import com.ihcy.integrate.entity.UserEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class RedisTest {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Test
    public void Test() {
        UserEntity userEntity1 = new UserEntity();
        userEntity1.setId(1L);

        UserEntity userEntity2 = new UserEntity();
        userEntity2.setId(2L);

        UserEntity userEntity3 = new UserEntity();
        userEntity3.setId(3L);
        redisTemplate.opsForHash().put("test", "user1", userEntity1);
        redisTemplate.opsForHash().put("test", "user2", userEntity2);
        redisTemplate.opsForHash().put("test", "user3", userEntity3);


        UserEntity user1 = (UserEntity) redisTemplate.opsForHash().get("test", "user1");
        UserEntity user2 = (UserEntity) redisTemplate.opsForHash().get("test", "user2");
        UserEntity user3 = (UserEntity) redisTemplate.opsForHash().get("test", "user3");

        System.out.println(user1);
        System.out.println(user2);
        System.out.println(user3);
    }

}
