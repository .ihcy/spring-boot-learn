package com.ihcy.integrate.mybatisplus;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ihcy.integrate.entity.UserEntity;
import com.ihcy.integrate.service.IUserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

/**
 * @ClassName UserServiceTest
 * @Description
 * @Author ihcy
 * @Date 2019/12/4 15:49
 * @Version 1.0
 **/
@SpringBootTest
@RunWith(SpringRunner.class)
public class UserServiceTest {

    @Autowired
    private IUserService userService;

    @Test
    public void testGetInfo() {
        UserEntity entity = userService.getById(1);
        System.out.println(entity);
    }

    @Test
    public void testGetList() {
        List<UserEntity> userEntities = userService.list();
        System.out.println(JSON.toJSONString(userEntities));

        // 需要在config中配置分页插件
        IPage<UserEntity> page = new Page<>();
        // 当前页
        page.setCurrent(2);
        // 每条页数
        page.setSize(2);
        page = userService.page(page);
        System.out.println(JSON.toJSONString(page.getRecords()));

        // 根据指定的字段查询用户信息集合

        Map<String, Object> map = new HashMap<>();
        // key是字段名，value是字段值
        map.put("age", 18);
        Collection<UserEntity> userEntities1 = userService.listByMap(map);
        System.out.println(JSON.toJSONString(userEntities1));
    }

    @Test
    public void testSave() {
        UserEntity entity = new UserEntity();
        entity.setAge(28);
        entity.setName("ihcy");
        entity.setEmail("ihcy@163.com");
        userService.save(entity);

        List<UserEntity> list = new ArrayList<>(10);
        list.add(new UserEntity(null, "Harray", 30, "Harray@163.com"));
        list.add(new UserEntity(null, "Luo", 15, "luo@163.com"));
        list.add(new UserEntity(null, "test", 151, "test.com"));
        userService.saveBatch(list);
    }

    @Test
    public void testUpate() {
        // 根据实体中的ID 去更新，其他字段值为null，则不会更新该字段，参考config
        UserEntity entity = new UserEntity();
        entity.setId(1L);
        entity.setName("1");
        userService.updateById(entity);

        UserEntity entity1 = new UserEntity();
        entity1.setAge(280);
        entity1.setName("ihcy");
        userService.updateById(entity1);


    }

    @Test
    public void testSaveOrUpdate() {
        // 传入的实体类ID为null,
        UserEntity entity2 = new UserEntity();
        entity2.setName("id is null");
        userService.saveOrUpdate(entity2);

        // 有ID 修改，无ID 保存
        List<UserEntity> list = new ArrayList<>();
        list.add(new UserEntity(1L, "杨波", 27, "qq.com"));
        list.add(new UserEntity(null, "测试无ID", 1, "1"));
        userService.saveOrUpdateBatch(list);

    }
    @Test
    public void testDelete() {
        // 根据单个ID 删除
        userService.removeById(1L);
        // 根据ID 批量删除
        userService.removeByIds(Arrays.asList(1L, 2L));

        // 根据字段删除用户信息
        Map<String,Object> map = new HashMap<>();
        map.put("name","id is null");
        userService.removeByMap(map);
    }

    @Test
    public void testQueryWrapper(){

        Map<String,Object> resutl = new HashMap<>();

        QueryWrapper<UserEntity> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.lambda().eq(UserEntity::getAge,18);
        List<UserEntity> userInfoEntityList1 = userService.list(queryWrapper1);
    }
}
