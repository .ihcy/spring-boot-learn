## bean copy
> 资料来源 https://juejin.im/post/5dd672e2e51d4536d737d504
    
希望主要可以实现：
1. 属性名称映射   
    origin.id->target.oId
2. 类型的转换  
    int 自动转为Integer；String->data 设置格式
3. 忽略拷贝  
    不拷贝某个值
4. 递归拷贝
    origin 中的实体类 拷贝到 target中的实体类
5. 数组和集合拷贝  
    list->list 主要是集合

对于常用的工具类分析

   
对于Java的后台实体类(DTO、DO、PO、VO)转换，简单试验。
 * Orika
 * JMapper
 * MapStruct