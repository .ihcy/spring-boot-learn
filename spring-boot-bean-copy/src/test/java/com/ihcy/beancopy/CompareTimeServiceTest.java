package com.ihcy.beancopy;

import com.ihcy.beancopy.time.CompareTimeService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
@Slf4j
public class CompareTimeServiceTest {
    @Autowired
    private CompareTimeService compareTimeService;

    @Test
    public void testTen(){
        log.info("orika 10个 时间{}",compareTimeService.orikaTime(10,10));
        log.info("mapper 10个 时间{}",compareTimeService.mapperTime(10,10));
    }
    @Test
    public void testHundred(){
        log.info("orika 100 时间{}",compareTimeService.orikaTime(100,100));
        log.info("mapper 100个 时间{}",compareTimeService.mapperTime(100,100));
    }
    @Test
    public void testThousand(){
        log.info("orika 1000个 时间{}",compareTimeService.orikaTime(1000,1000));
        log.info("mapper 1000个 时间{}",compareTimeService.mapperTime(1000,1000));
    }
}
