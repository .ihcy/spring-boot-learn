package com.ihcy.beancopy;

import com.ihcy.beancopy.service.IGeneratorDataService;
import com.ihcy.beancopy.service.IMapStructService;
import com.ihcy.beancopy.service.IOrikaService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ApplicationTests {

    @Autowired
    private IOrikaService orikaService;

    @Autowired
    private IMapStructService mapStructService;

    @Autowired
    private IGeneratorDataService generatorDataService;

    @Test
    public void testOrder(){
        Assert.assertTrue( generatorDataService.test(orikaService.getOrdersFromOrigin(),mapStructService.getOrdersFromOrigin()));
    }
    @Test
    public void testClass(){
        Assert.assertTrue( generatorDataService.test(orikaService.getClassesFromOrigin(),mapStructService.getClassesFromOrigin()));
    }

}
