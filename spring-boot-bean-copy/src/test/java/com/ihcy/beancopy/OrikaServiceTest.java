package com.ihcy.beancopy;

import com.ihcy.beancopy.dto.TargetOrder;
import com.ihcy.beancopy.service.IOrikaService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @ClassName OrikaServiceTest
 * @Description
 * @Author ihcy
 * @Date 2020/1/14 20:27
 * @Version 1.0
 **/
@SpringBootTest
@RunWith(SpringRunner.class)
public class OrikaServiceTest {

    @Autowired
    private IOrikaService orikaService;


    @Test
    public void testOrder(){
        List<TargetOrder> targetOrders = orikaService.getOrdersFromOrigin();
        targetOrders.get(0).getItems().get(0).setItemName("isUpdate");

     }
    @Test
     public void testClass(){
         orikaService.getClassesFromOrigin();
     }
}
