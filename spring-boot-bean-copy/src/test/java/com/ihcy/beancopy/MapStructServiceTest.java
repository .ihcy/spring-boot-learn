package com.ihcy.beancopy;

import com.ihcy.beancopy.dto.TargetOrder;
import com.ihcy.beancopy.service.IMapStructService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @ClassName MapStructServiceTest
 * @Description
 * @Author ihcy
 * @Date 2020/1/14 21:24
 * @Version 1.0
 **/
@SpringBootTest
@RunWith(SpringRunner.class)
public class MapStructServiceTest {

    @Autowired
    private IMapStructService mapStructService;

    @Test
    public void getOrdersFromOrigin(){
        List<TargetOrder> targetOrders = mapStructService.getOrdersFromOrigin();
        targetOrders.get(0).getItems().get(0).setItemName("isUpdate");
    }

    @Test
    public void getClassesFromOrigin(){
        mapStructService.getClassesFromOrigin();
    }
}
