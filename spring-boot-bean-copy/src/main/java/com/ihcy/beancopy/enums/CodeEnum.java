package com.ihcy.beancopy.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum CodeEnum {

    /**
     * test
     */
    ONE(1,"1"),
    TWO(2,"2"),
    THREE(3,"3");
    private Integer number;

    private String code;

}
