package com.ihcy.beancopy.time;

import com.ihcy.beancopy.dto.OriginOrder;
import com.ihcy.beancopy.dto.TargetOrder;
import com.ihcy.beancopy.mapstruct.ClassMapper;
import com.ihcy.beancopy.mapstruct.OrderMapper;
import com.ihcy.beancopy.service.IGeneratorDataService;
import ma.glasnost.orika.MapperFactory;
import org.omg.CORBA.INTERNAL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 计算不同框架的调用时间
 */
@Service
public class CompareTimeService {

    @Autowired
    private MapperFactory mapperFactory;

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private ClassMapper classMapper;

    @Autowired
    private IGeneratorDataService generatorDataService;

    public Long orikaTime(Integer num1, Integer num2) {
        Long time = System.currentTimeMillis();
        List<OriginOrder> originOrders = generatorDataService.getOriginOrders(num1, num2);
        List<TargetOrder> targetOrders = mapperFactory.getMapperFacade().mapAsList(originOrders, TargetOrder.class);
        return System.currentTimeMillis() - time;
    }

    public Long mapperTime(Integer num1 , Integer num2){
        Long time = System.currentTimeMillis();
        List<OriginOrder> originOrders = generatorDataService.getOriginOrders(num1, num2);
        List<TargetOrder> targetOrders = orderMapper.clone(originOrders);
        return System.currentTimeMillis() - time;
    }
}
