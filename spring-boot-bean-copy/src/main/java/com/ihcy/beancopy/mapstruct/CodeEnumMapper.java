package com.ihcy.beancopy.mapstruct;

import com.ihcy.beancopy.enums.CodeEnum;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * @ClassName CodeEnumMapper
 * @Description
 * @Author ihcy
 * @Date 2020/1/14 21:14
 * @Version 1.0
 **/
@Component
public class CodeEnumMapper {

    public Integer enumToInteger(CodeEnum codeEnum) {
        return codeEnum.getNumber();
    }

    public CodeEnum integerToEnum (Integer code) {
        CodeEnum codeEnum = null;
        for (CodeEnum value : CodeEnum.values()) {
            if (value.getNumber().equals(code)) {
                codeEnum = value;
            }
        }
        return codeEnum;
    }
}
