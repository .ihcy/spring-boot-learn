package com.ihcy.beancopy.mapstruct;

import com.ihcy.beancopy.dto.OriginOrder;
import com.ihcy.beancopy.dto.TargetOrder;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;
/**
 * 字段相同
 */
@Mapper(componentModel="spring",uses ={DateMapper.class} )
public interface OrderMapper {


    @Mapping(source = "name1",target = "name2")
    TargetOrder clone(OriginOrder order);

    List<TargetOrder> clone(List<OriginOrder> orders);



}