package com.ihcy.beancopy.mapstruct;

import com.ihcy.beancopy.dto.*;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel="spring")
public interface TempMapper {

    @Mappings({
            @Mapping(source = "itemList",target = "items"),
            @Mapping(source = "order.nameTemp",target = "name2"),
            @Mapping(source = "demo.nameTemp",target = "demo.name")
    })
    TargetOrder clone(TempOrder order, TempDemo demo, List<TempItem> itemList);

    TargetOrder.TargetOrderItem clone(TempItem item, List<ItemDemoTemp> itemDemos);
}
