package com.ihcy.beancopy.service;

import com.ihcy.beancopy.dto.TargetClass;
import com.ihcy.beancopy.dto.TargetOrder;

import java.util.List;

public interface IOrikaService {


    List<TargetOrder> getOrdersFromOrigin();

    List<TargetClass> getClassesFromOrigin();
}
