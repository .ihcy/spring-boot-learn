package com.ihcy.beancopy.service.convert;

import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class StringDateConverter  extends BidirectionalConverter<Date,String> {

    @Override
    public String convertTo(Date source, Type<String> destinationType, MappingContext mappingContext) {
        return new SimpleDateFormat("yyyy-MM-dd").format(source);
    }

    @Override
    public Date convertFrom(String source, Type<Date> destinationType, MappingContext mappingContext) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(source);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}
