package com.ihcy.beancopy.service.impl;

import com.ihcy.beancopy.dto.OriginClass;
import com.ihcy.beancopy.dto.OriginOrder;
import com.ihcy.beancopy.dto.TargetClass;
import com.ihcy.beancopy.dto.TargetOrder;
import com.ihcy.beancopy.mapstruct.ClassMapper;
import com.ihcy.beancopy.mapstruct.OrderMapper;
import com.ihcy.beancopy.service.IGeneratorDataService;
import com.ihcy.beancopy.service.IMapStructService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MapStructServiceImpl implements IMapStructService {

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private ClassMapper classMapper;

    @Autowired
    private IGeneratorDataService generatorDataService;


    @Override
    public List<TargetOrder> getOrdersFromOrigin() {
        List<OriginOrder> originOrders = generatorDataService.getOriginOrders();
        List<TargetOrder> targetOrders = orderMapper.clone(originOrders);
        System.out.println(generatorDataService.test(originOrders,targetOrders));
        return targetOrders;
    }

    @Override
    public List<TargetClass> getClassesFromOrigin() {
        List<OriginClass> originClasses = generatorDataService.getOriginClasses();
        List<TargetClass> targetClasses =classMapper.clone(originClasses);
        System.out.println(generatorDataService.test(originClasses,targetClasses));
        return targetClasses;
    }
}
