package com.ihcy.beancopy.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.ihcy.beancopy.dto.OriginClass;
import com.ihcy.beancopy.dto.OriginOrder;
import com.ihcy.beancopy.service.IGeneratorDataService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class GeneratorDataServiceImpl implements IGeneratorDataService {

    @Override
    public List<OriginOrder> getOriginOrders() {
        return getOriginOrders(10,5);
    }

    @Override
    public List<OriginOrder> getOriginOrders(Integer num1, Integer num2) {
        List<OriginOrder> originOrders = new ArrayList<>();
        for (int i = 0; i < num1; i++) {
            OriginOrder originOrder = new OriginOrder(
                    i, "order_" + i, "address_" + i, new Date(), "customer" + i, i / 2 == 0
            );
            List<OriginOrder.OriginOrderItem> items = new ArrayList<>();
            for (int j = 0; j < num2; j++) {
                OriginOrder.OriginOrderItem item = new OriginOrder.OriginOrderItem(
                        j, originOrder.getOrderNo() + "j", "item" + i + j, j, originOrder.getCreatedTime()
                );
                items.add(item);
            }
            originOrder.setItems(items);
            originOrder.setName1("name" + i);
            originOrder.setIntegerToString(i);
            originOrder.setStringToInteger("" + i);
            originOrder.setDateToString(new Date());
            originOrder.setStringToDate("2019-01-0" + (i + 1));
            originOrder.setDemo(new OriginOrder.OriginDemo("testDemo" + i));
            originOrders.add(originOrder);
        }
        return originOrders;
    }


    @Override
    public List<OriginClass> getOriginClasses() {
        return getOriginClasses(10,5);
    }

    @Override
    public List<OriginClass> getOriginClasses(Integer num1, Integer num2) {
        List<OriginClass> originClasses = new ArrayList<>();
        for (int i = 0; i < num1; i++) {
            OriginClass originClass = new OriginClass();
            originClass.setClassid("" + i);
            originClass.setClassname("name" + i);
            originClass.setDate("2019-01-0" + (i + 1));
            originClass.setNum("" + (i * 5));
            originClass.setCode(i);
            originClass.setTea(new OriginClass.Tea("tea" + i, "" + i));
            List<OriginClass.Std> stds = new ArrayList<>();
            originClass.setStds(stds);
            for (int j = 0; j < num2; j++) {
                OriginClass.Std std = new OriginClass.Std("std" + i + j, "" + i + j, "2020-01-02");
                stds.add(std);
            }
            originClasses.add(originClass);
        }
        return originClasses;
    }

    @Override
    public <K, V> Boolean test(K k, V v) {
        String kJson = JSON.toJSONString(k, SerializerFeature.WriteDateUseDateFormat, SerializerFeature.SortField);
        String vJson = JSON.toJSONString(v, SerializerFeature.WriteDateUseDateFormat, SerializerFeature.SortField);
        System.out.println(kJson);
        System.out.println(vJson);
        return kJson.equals(vJson);
    }
}
