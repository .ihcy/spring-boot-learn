package com.ihcy.beancopy.service;

import com.ihcy.beancopy.dto.OriginClass;
import com.ihcy.beancopy.dto.OriginOrder;

import java.util.List;

public interface IGeneratorDataService {

    List<OriginOrder> getOriginOrders();
    List<OriginOrder> getOriginOrders(Integer num1,Integer num2);

    List<OriginClass> getOriginClasses();
    List<OriginClass> getOriginClasses(Integer num1,Integer num2);

    <K, V> Boolean test(K k, V v);
}
