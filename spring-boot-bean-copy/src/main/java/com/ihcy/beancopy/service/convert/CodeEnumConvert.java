package com.ihcy.beancopy.service.convert;


import com.ihcy.beancopy.enums.CodeEnum;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;
import org.springframework.stereotype.Service;

@Service
public class CodeEnumConvert extends BidirectionalConverter<CodeEnum, Integer> {

    @Override
    public Integer convertTo(CodeEnum source, Type<Integer> destinationType, MappingContext mappingContext) {
        return source.getNumber();
    }

    @Override
    public CodeEnum convertFrom(Integer source, Type<CodeEnum> destinationType, MappingContext mappingContext) {
        CodeEnum codeEnum = null;
        for (CodeEnum value : CodeEnum.values()) {
            if (value.getNumber().equals(source)) {
                codeEnum = value;
            }
        }
        return codeEnum;
    }
}
