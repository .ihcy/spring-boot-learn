package com.ihcy.beancopy.dto;

import com.ihcy.beancopy.enums.CodeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TargetClass {

    private String id;

    private String className;

    private Date time;

    private Integer number;

    private List<Student> students;

    private CodeEnum codeEnum;

    private Teacher teacher;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Student{

        private String name;

        private Long id;

        private Date time;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Teacher{
        private String name;

        private Long id;
    }
}
