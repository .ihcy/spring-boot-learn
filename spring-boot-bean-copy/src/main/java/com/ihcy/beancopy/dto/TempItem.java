package com.ihcy.beancopy.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TempItem {
    private Integer id;

    private String orderItemNo;

    private String itemName;

    private Integer num;

    private Date createdTime;
}
