package com.ihcy.beancopy.dto;

import com.ihcy.beancopy.enums.CodeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * @ClassName TargetOrder
 * @Description
 * @Author ihcy
 * @Date 2020/1/12 17:14
 * @Version 1.0
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TargetOrder {

    private Integer id;

    private String orderNo;

    private String address;

    private Date createdTime;

    private String creator;

    private Boolean flag;

    private Integer stringToInteger;

    private String integerToString;

    private Date stringToDate;

    private String dateToString;

    private CodeEnum code;

    private String name2;

    private TargetDemo demo;

    private List<TargetOrderItem> items;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class TargetDemo{
        private String name;

    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class TargetOrderItem{

        private Integer id;

        private String orderItemNo;

        private String itemName;

        private Integer num;

        private Date createdTime;
    }
}
