package com.ihcy.beancopy.dto;

import com.ihcy.beancopy.enums.CodeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TempOrder {
    private Integer id;

    private String orderNo;

    private String address;

    private Date createdTime;

    private String creator;

    private Boolean flag;

    private Integer stringToInteger;

    private String integerToString;

    private Date stringToDate;

    private String dateToString;

    private CodeEnum code;

    private String nameTemp;
}
