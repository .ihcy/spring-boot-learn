package com.ihcy.beancopy.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ItemDemoTemp {
    private Integer id;

    private String orderItemNo;

    private String itemName;

    private Integer num;
}
