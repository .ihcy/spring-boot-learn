package com.ihcy.beancopy.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OriginClass {

    private String classid;

    private String classname;

    private String date;

    private String num;

    private List<Std> stds;

    private Integer code;

    private Tea tea;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Std{

        private String stdname;

        private String stdid;

        private String time;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Tea{
        private String name;

        private String id;
    }
}
